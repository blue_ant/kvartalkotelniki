let cardsListBuilder = new PlanCardList();
let favList = new FavoritesList();
let $listCont = $("#flatTypesResultListContainer");

new FilterForm("#filterForm", {
	submitHandler: $.noop
});

$.ajax({
	url: "https://kvartalkotelniki.ru/choose/params/",
	//url: '/ajax/get_typicals_example.json',
	dataType: "json",
	method: "GET",
	data: {
		action: "get_typicals"
	}
})
	.done(jsonResponse => {
		let favPlans = jsonResponse;

		//---- filter favorites
		favPlans = _.filter(favPlans, pln => {
			return _.includes(favList.favorites, pln.id);
		});

		// mark all plans as favorite
		favPlans.forEach(pln => {
			pln.favorite = 1;
		});

		// render flat list
		if (favPlans.length) {
			$listCont.html(cardsListBuilder.renderCards(favPlans));
		} else {
			$listCont.html("Нет квартир в избранном...");
		}
	})
	.fail(() => {
		alert("Не удалось получить данные!\nПопробуйте позже или обратитесь к администрации сайта.");
	});

$("[data-toggle-filter-panel]").on("click", event => {
	event.preventDefault();
	$(".FiltersSection_rightCol").toggleClass("FiltersSection_rightCol-active");
});

// START: favorites toggling behaviour

$listCont.on("click", ".PlanCard_ico-star", event => {
	event.preventDefault();
	let $favLink = $(event.currentTarget);
	let $card = $favLink.parents(".PlanCard").eq(0);

	$card.fadeOut("100", () => {
		$card.remove();
		if ($listCont.find(".PlanCard").length === 0) {
			$listCont.html("Нет квартир в избранном...");
		}
	});

	favList.remove($favLink.data("flat-type-id"));
});

// END: favorites toggling behaviour
