let homepageSlideshow = new Slideshow("#homepageSlideshow");

new Swiper("#homepageCardsMinislider_1", {
	spaceBetween: 18
});

// START: minigenplan
{
	let $minigenplanPushImg = $("#minigenplanShapePusherImg");
	let newSrc = `${$minigenplanPushImg.attr("src")}?ts=${Date.now()}`;
	$minigenplanPushImg.attr("src", newSrc).on("load", event => {
		event.preventDefault();
		new Fitblock($(".Minigenplan .Fitblock").get(0));
	});
}
// END: minigenplan

new AboutProjectSection("#AboutProjectSection");

// START: news block mobile slider
{
	let $homepageNewsSection = $(".NewsSection_mobileNewsBlock");
	let newsMobileSlider = new Swiper($homepageNewsSection.find(".swiper-container"), {
		navigation: {
			nextEl: $homepageNewsSection.find(".DotsPager_arrow-right"),
			prevEl: $homepageNewsSection.find(".DotsPager_arrow-left"),
			disabledClass: "DotsPager_arrow-disabled"
		},
		pagination: {
			type: "custom",
			el: $homepageNewsSection.find(".DotsPager_dotsWrap"),
			renderCustom: (swiper, current, total) => {
				return DotsPager.prototype.renderDots(current, total);
			}
		}
		//allowTouchMove: false,
	});

	/*let titlesSwitcher = new Swiper($homepageNewsSection.find(".NewsSection_mobileLinksSlider"), {
		effect: "fade",
		allowTouchMove: false,
		autoHeight: true
	});

	newsMobileSlider.controller.control = titlesSwitcher;*/
}
// END: news block mobile slider

// START: homepage map

let yandexMap;
let yandexMarkers = window.ifrasObjsYandexMarkers;

ymaps.ready(init);
function init() {
	yandexMap = new ymaps.Map("Map", {
    center: [55.653465, 37.823784],
    zoom: 13,
  });

  let markerCollection, multiRoute;

  function drawMarkers (markers) {
		// Создание макета балуна
	  let MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
	    '<div class="Balloon Balloon-top">' +
	    '<a class="Balloon_close" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/><path d="M0 0h24v24H0z" fill="none"/></svg></a>' +
	    '<div class="Balloon_arrow"></div>' +
	    '<div class="Balloon_inner">' +
	    '$[[options.contentLayout observeSize maxHeight=350]]' +
	    '</div>' +
	    '</div>', {
	    build: function () {
	      this.constructor.superclass.build.call(this);
	      this._$element = $('.Balloon', this.getParentElement());
	      this.applyElementOffset();
	      this._$element.find('.Balloon_close').on('click', $.proxy(this.onCloseClick, this));
	    },

	    clear: function () {
	      this._$element.find('.Balloon_close').off('click');
	      this.constructor.superclass.clear.call(this);
	    },

	    onSublayoutSizeChange: function () {
	      MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);
	      if(!this._isElement(this._$element)) {
	      	return;
	      }
	      this.applyElementOffset();
	      this.events.fire('shapechange');
	    },

	    applyElementOffset: function () {
	      this._$element.css({
	        left: -(this._$element[0].offsetWidth / 2),
	        top: -(this._$element[0].offsetHeight + this._$element.find('.Balloon_arrow')[0].offsetHeight)
	      });
	    },

	    onCloseClick: function (e) {
	      e.preventDefault();
	      this.events.fire('userclose');
	    },

	    getShape: function () {
	      if(!this._isElement(this._$element)) {
	        return MyBalloonLayout.superclass.getShape.call(this);
	      }

	      var position = this._$element.position();

	      return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
	        [position.left, position.top], [
	          position.left + this._$element[0].offsetWidth,
	          position.top + this._$element[0].offsetHeight + this._$element.find('.Balloon_arrow')[0].offsetHeight
	        ]
	      ]));
	    },

	    _isElement: function (element) {
	      return element && element[0] && element.find('.Balloon_arrow')[0];
	    }
	  });

		// Создание вложенного макета содержимого балуна.
	  let MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
	    '<h3 class="Balloon_title">$[properties.balloonHeader]</h3>' +
	    '<div class="Balloon_content">$[properties.balloonContent]</div>'
	  );

		markerCollection = new ymaps.GeoObjectCollection({}, {
			iconLayout: 'default#image',
			hideIconOnBalloonOpen: false,
		});

		yandexMarkers.forEach(item => {
			if (item.hasOwnProperty('flag')) {
				markerCollection.add(new ymaps.Placemark([item.coords.lat, item.coords.lng], {
		    	hintContent: item.title,
		    	markerType: item.type,
		    	flag: item.flag,
		    	ico2: item.ico2
		    }, {
		    	iconImageHref: item.ico,
		    	iconImageSize: [44, 46]
		    }));
			} else {
				markerCollection.add(new ymaps.Placemark([item.coords.lat, item.coords.lng], {
		    	hintContent: item.title,
		    	balloonContent: item.title,
		    	markerType: item.type,
		    	flag: item.flag,
		    	ico2: item.ico2
		    }, {
		    	iconImageHref: item.ico,
		    	iconImageSize: [44, 46],
		    	balloonOffset: [8, -24],
		    	balloonLayout: MyBalloonLayout,
		    	balloonContentLayout: MyBalloonContentLayout,
		    }));
			}
		});

		markerCollection.add(new ymaps.Placemark([55.638661, 37.845971], {
	  	hintContent: "ЖК «Новые Котельники»",
	  	balloonContent: "ЖК «Новые Котельники»",
	  }, {
	  	iconImageHref: "/img/mappins/logo_mark.png",
	  	iconImageSize: [74, 75],
	  	balloonOffset: [23, -24],
	  	balloonLayout: MyBalloonLayout,
	  	balloonContentLayout: MyBalloonContentLayout,
	  }));

		yandexMap.geoObjects.add(markerCollection);
	}

  function hideYandexMarkers (type) {
  	markerCollection.each(item => {
	  	console.log(item.properties.getAll());
	  	if (type === item.properties.get('markerType')) {
	  		item.options.set('visible', false);
	  	}
	  });
  }

  function showYandexMarkers (type) {
  	markerCollection.each(item => {
	  	console.log(item.properties.getAll());
	  	if (type === item.properties.get('markerType')) {
	  		item.options.set('visible', true);
	  	}
	  });
  }

  function setDefaultMarkerImage () {
  	markerCollection.each(item => {
  		if (item.properties.get('flag')) {
  			if (item.properties.get('flag') == 2) {
  				let bufferImg = item.properties.get('ico2');
					item.properties.set('ico2', item.options.get('iconImageHref'));
					item.options.set('iconImageHref', bufferImg);
					item.options.set('iconImageSize', [44, 46]);
					item.options.set('iconImageOffset', [-12, -40]);
					item.properties.set('flag', 1);
  			}
  		}
  	});
  }

	drawMarkers(yandexMarkers);

	// При клике на карту все метки будут удалены.
	markerCollection.getMap().events.add('click', function() {
    console.log('Click on map!');
	});

	markerCollection.each(item => {
  	item.events.add('click', function (e) {
  		console.log(item.options.get('iconImageHref'));
  		if (item.properties.get('flag')) {
  			if (item.properties.get('flag') == 1) {
  				setDefaultMarkerImage();
  				let bufferImg = item.options.get('iconImageHref');
  				item.options.set('iconImageHref', item.properties.get('ico2'));
  				item.properties.set('ico2', bufferImg);
					item.options.set('iconImageSize', [230, 240]);
					item.options.set('iconImageOffset', [-110, -240]);
					item.properties.set('flag', 2);
  			} else if (item.properties.get('flag') == 2) {
  				let bufferImg = item.properties.get('ico2');
  				item.properties.set('ico2', item.options.get('iconImageHref'));
  				item.options.set('iconImageHref', bufferImg);
					item.options.set('iconImageSize', [44, 46]);
					item.options.set('iconImageOffset', [-12, -40]);
					item.properties.set('flag', 1);
  			}
	  	}
  	});
  });

	$("[data-marker-type]").click(function() {
		let markerType = $(this).data("marker-type");

		$(this).toggleClass("isOff");

		if ($(this).hasClass("isOff")) {
			hideYandexMarkers(+markerType);
		} else {
			showYandexMarkers(+markerType);
		}
	});

	yandexMap.controls.remove('geolocationControl');
  yandexMap.controls.remove('searchControl');
  yandexMap.controls.remove('trafficControl');
  yandexMap.controls.remove('typeSelector');
  yandexMap.controls.remove('fullscreenControl');
  yandexMap.controls.remove('rulerControl');
  yandexMap.behaviors.disable(['scrollZoom']);
}

// END: homepage map

// START: Homepage accordion

{
	$(".Minigenplan_list").accordion({
		header: ".Minigenplan_listLink",
		icons: false,
		active: false,
		collapsible: true,
		animate: 200,
		classes: {
			"ui-accordion-header-active": "Minigenplan_listLink-active"
		},
		beforeActivate: (event, ui) => {
			let markerClassActive = "Minigenplan_marker-active";

			let id = $(ui.newHeader).data("trigger-marker");

			$(`.Minigenplan_marker`)
				.removeClass(markerClassActive)
				.filter(`[data-marker="${id}"]`)
				.addClass(markerClassActive);
		},
		create: () => {
			let $markers = $(`.Minigenplan_marker`);
			let $tabs = $(".Minigenplan_listLink");
			let markerClassActive = "Minigenplan_marker-active";

			$markers.click(event => {
				event.preventDefault();

				let $el = $(event.currentTarget);
				let id = $el.data("marker");

				if ($tabs.filter(`[data-trigger-marker="${id}"]`).length === 0) return;

				$markers.removeClass(markerClassActive);
				$el.addClass(markerClassActive);

				$tabs.filter(`[data-trigger-marker="${id}"]`).trigger("click");
			});
		},

		activate: (event, ui) => {
			$(ui.newPanel).removeAttr("style");
		}
	});
}

// END: Homepage accordion

new InteractiveForm(document.querySelector(".FormPopup_form"));

new Countdown("#homepageCoundown");

// Card Stack Effect
let yuda = new Stack(document.getElementById("stack_yuda"));

if ($("#stack_yuda .stack__item").length > 1) {
	setInterval(function() {
		yuda.reject();
	}, 7000);
}
