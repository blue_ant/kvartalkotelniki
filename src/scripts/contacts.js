/* Yandex Map */
let map;
let markers = [
	{
		coords: [55.638661, 37.845971],
		title: "ЖК «Новые Котельники»",
		balloonOffset: [23, -24],
		icon: {
			url: "/img/mappins/logo_mark.png",
			size: [74, 75]
		}
	},
	{
		coords: [55.785674, 37.240471],
		title: "Концерн «Русич»",
		balloonOffset: [23, -24],
		icon: {
			url: "/img/mappins/rusich_dot_map.png",
			size: [74, 75]
		}
	},
	{
		coords: [55.67372, 37.859038],
		title: "метро «Котельники»",
		balloonOffset: [9, -24],
		icon: {
			url: "/img/mappins/metro.png",
			size: [45, 45]
		}
	},
	{
		coords: [55.632815, 37.766049],
		title: "метро «Алма-Атинская»",
		balloonOffset: [9, -24],
		icon: {
			url: "/img/mappins/metro.png",
			size: [45, 45]
		}
	}
];
ymaps.ready(init);
function init() { 
  map = new ymaps.Map("Map", {
    center: [55.653465, 37.823784],
    zoom: 13,
  });

  let markerCollection, multiRoute;

  function drawMarkers (markers) {
		// Создание макета балуна
	  let MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
	    '<div class="Balloon Balloon-top">' +
	    '<a class="Balloon_close" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/><path d="M0 0h24v24H0z" fill="none"/></svg></a>' +
	    '<div class="Balloon_arrow"></div>' +
	    '<div class="Balloon_inner">' +
	    '$[[options.contentLayout observeSize maxWidth=235 maxHeight=350]]' +
	    '</div>' +
	    '</div>', {
	    build: function () {
	      this.constructor.superclass.build.call(this);
	      this._$element = $('.Balloon', this.getParentElement());
	      this.applyElementOffset();
	      this._$element.find('.Balloon_close').on('click', $.proxy(this.onCloseClick, this));
	    },

	    clear: function () {
	      this._$element.find('.Balloon_close').off('click');
	      this.constructor.superclass.clear.call(this);
	    },

	    onSublayoutSizeChange: function () {
	      MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);
	      if(!this._isElement(this._$element)) {
	      	return;
	      }
	      this.applyElementOffset();
	      this.events.fire('shapechange');
	    },

	    applyElementOffset: function () {
	      this._$element.css({
	        left: -(this._$element[0].offsetWidth / 2),
	        top: -(this._$element[0].offsetHeight + this._$element.find('.Balloon_arrow')[0].offsetHeight)
	      });
	    },

	    onCloseClick: function (e) {
	      e.preventDefault();
	      this.events.fire('userclose');
	    },

	    getShape: function () {
	      if(!this._isElement(this._$element)) {
	        return MyBalloonLayout.superclass.getShape.call(this);
	      }

	      var position = this._$element.position();

	      return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
	        [position.left, position.top], [
	          position.left + this._$element[0].offsetWidth,
	          position.top + this._$element[0].offsetHeight + this._$element.find('.Balloon_arrow')[0].offsetHeight
	        ]
	      ]));
	    },

	    _isElement: function (element) {
	      return element && element[0] && element.find('.Balloon_arrow')[0];
	    }
	  });

		// Создание вложенного макета содержимого балуна.
	  let MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
	    '<h3 class="Balloon_title">$[properties.balloonHeader]</h3>' +
	    '<div class="Balloon_content">$[properties.balloonContent]</div>'
	  );

		markerCollection = new ymaps.GeoObjectCollection({}, {
			iconLayout: 'default#image',
			hideIconOnBalloonOpen: false,
		});

		markers.forEach(item => {
	    markerCollection.add(new ymaps.Placemark(item.coords, {
	    	hintContent: item.title,
	    	balloonContent: item.title,
	    }, {
	    	iconImageHref: item.icon.url,
	    	iconImageSize: item.icon.size,
	    	balloonOffset: item.balloonOffset,
	    	balloonLayout: MyBalloonLayout,
	    	balloonContentLayout: MyBalloonContentLayout,
	    }));
		});

		map.geoObjects.add(markerCollection);
	}

	drawMarkers(markers);

	map.events.add('click', function(e) {
    let coords = e.get('coords');

    let clickPosition = {
    	lat: coords[0],
    	lng: coords[1]
    };

    markerCollection.removeAll();
    map.geoObjects.remove(multiRoute);
    drawMarkers(markers);

    markerCollection.add(new ymaps.Placemark([clickPosition.lat, clickPosition.lng], {
    	hintContent: "Точка отправления"
    }, {
      iconLayout: 'default#image',
    	iconImageHref: "/img/mappins/dest-icon.svg",
    	iconImageSize: [18, 18]
    }));

    markerCollection.add(new ymaps.Placemark([55.633123, 37.851133], {
    	hintContent: "Точка назначения ЖК Котельники"
    }, {
      iconLayout: 'default#image',
    	iconImageHref: "/img/mappins/dest-pulse-icon.svg",
    	iconImageSize: [90, 90]
    }));

    // Построение маршрута.
		multiRoute = new ymaps.multiRouter.MultiRoute({   
	    referencePoints: [
        [clickPosition.lat, clickPosition.lng],
        [55.638434, 37.857576],
	    ]
		}, {
	      routeActiveStrokeWidth: 4,
	      routeActiveStrokeColor: "#90CEAA"
		});

		// Добавление маршрута на карту.
		map.geoObjects.add(multiRoute);
	});

  map.controls.remove('geolocationControl');
  map.controls.remove('searchControl');
  map.controls.remove('trafficControl');
  map.controls.remove('typeSelector');
  map.controls.remove('fullscreenControl');
  map.controls.remove('rulerControl');
  map.behaviors.disable(['scrollZoom']);
}

$("[data-bind-marker]").click(function() {
	let order = $(this).data("bind-marker");
	map.setCenter([markers[order].coords[0], markers[order].coords[1]]);
});

$(".MapLocations").accordion({
	header: "h3",
	active: 0,
	heightStyle: "content",
	collapsible: true,
	animate: 300,
	create: function(/*event, ui*/) {
		for (let i = 0; i < $(".MapLocations .MapLocations_header").length; i++) {
			$(`.MapLocations .MapLocations_header:eq(${i})`).append($(".MapLocations .MapLocations_headerIcon").get(i));
		}
		$(".MapLocations_wrapper").niceScroll({
			cursorcolor: "#A7A8AA",
			cursorfixedheight: 70,
			cursoropacitymin: 1,
			cursorwidth: 4,
			preventmultitouchscrolling: false,
			horizrailenabled: true,
			cursorborder: "0px solid transparent",
			scrollspeed: 200,
			cursorborderradius: "0"
		});
	}
});

new Swiper(".MapSlider", {
	slidesPerView: "auto",
	centeredSlides: true,
	spaceBetween: 15
});

$(".MapTip_close").click(function() {
	$(this)
		.parent(".MapTip")
		.addClass("MapTip-hidden");
});