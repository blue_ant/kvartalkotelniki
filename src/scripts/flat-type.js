let flatsListBuilder = new FlatTeaser();
let $flatListCont = $(".FlatsMinilist_listWrap");

let $filterForm = $("#filterForm");

new FilterForm($filterForm, {
	submitHandler: $filterForm => {
		$.ajax({
			url: "https://kvartalkotelniki.ru/choose/params/",
			//url: '/ajax/get_typicals_example.json',
			dataType: "json",
			method: "GET",
			localCache: true,
			cacheTTL: 1,
			data: {
				action: "get_typicals",
				typical: $filterForm.data("typical")
			}
		}).done(jsonResponse => {
			let flatsArray = jsonResponse[0].flats;
			// parse filters params to object
			let currentFiltersParams = prepareFilterFormValues($filterForm.serializeObject());

			// filter flats array
			//---- filter by price
			flatsArray = _.filter(flatsArray, flt => {
				let priceMin = currentFiltersParams.price_from;
				let priceMax = currentFiltersParams.price_to;
				return _.inRange(flt.price, priceMin, priceMax + 1);
			});
			//---- filter by floor
			flatsArray = _.filter(flatsArray, flt => {
				let highestFloor = currentFiltersParams.floor_to;
				let lowestFloor = currentFiltersParams.floor_from;
				return _.inRange(flt.floor, lowestFloor, highestFloor + 1);
			});

			//---- filter by house
			if (currentFiltersParams.house) {
				flatsArray = _.filter(flatsArray, flt => {
					let result = false;
					if (_.includes(currentFiltersParams.house, flt.house)) {
						result = true;
					}
					return result;
				});
			}
			//---- filter by side of view
			if (currentFiltersParams.sides) {
				flatsArray = _.filter(flatsArray, flt => {
					let result = false;
					if (_.intersection(currentFiltersParams.sides, flt.sides).length) {
						result = true;
					}
					return result;
				});
			}

			//---- filter by release date
			flatsArray = _.filter(flatsArray, flt => {
				let releaseDateFrom = currentFiltersParams.entry_date_from;
				let releaseDateTo = currentFiltersParams.entry_date_to;

				return _.inRange(flt.entryDate, releaseDateFrom, releaseDateTo + 1);
			});

			// ---- filter by finish type
			if (currentFiltersParams.finish_option !== undefined) {
				let activeType = currentFiltersParams.finish_option;
				const type = {
					0: 'finish-full',
					1: 'finish-white-box',
				};
				flatsArray = _.filter(flatsArray, flat => {
					return flat[type[activeType]];
				});
			}


			$flatListCont.html(flatsListBuilder.renderCards(flatsArray));
			saveFilterFormInputs();
		});
	}
});

$('#CheckboxGroup input:checkbox').click(function(){
	if ($(this).is(':checked')) {
		$('#CheckboxGroup input:checkbox').not(this).prop('checked', false);
	}
});

$("[data-toggle-filter-panel]").on("click", event => {
	event.preventDefault();
	$(".FiltersSection_rightCol").toggleClass("FiltersSection_rightCol-active");
});

$(".ba-slider").beforeAfter();

$("#makeOrderBtn").on("click", function(event) {
	event.preventDefault();
	$.fancybox.open($("#orderModal"), {
		autoCenter: true
	});
});

let favList = new FavoritesList();

let $starIco = $(".FlatTypeDescr_star");

if (_.includes(favList.favorites, $starIco.data("flat-type-id"))) {
	$starIco.addClass("FlatTypeDescr_star-active");
}

$starIco.on("click", function(event) {
	event.preventDefault();
	$starIco.toggleClass("FlatTypeDescr_star-active");
	if ($starIco.hasClass("FlatTypeDescr_star-active")) {
		favList.add($starIco.data("flat-type-id"));
	} else {
		favList.remove($starIco.data("flat-type-id"));
	}
});

new InteractiveForm(document.querySelector(".FormModal_form"));
