let genplanFitblock = new Fitblock($(".Fitblock-genplan").get(0));

let accordionWasInited = false;

function accordionInit() {
	$(".MapLocations").accordion({
		header: "h3",
		active: false,
		heightStyle: "auto",
		collapsible: true,
		animate: 300,
		create: function() {
			if (accordionWasInited == false) {
				for (let i = 0; i < $(".MapLocations .MapLocations_header").length; i++) {
					$(`.MapLocations .MapLocations_header:eq(${i})`).append($(".MapLocations .MapLocations_headerIcon").get(i));
				}
			}
			$(".MapLocations_wrapper").niceScroll({
				cursorcolor: "#A7A8AA",
				cursorfixedheight: 70,
				cursoropacitymin: 1,
				cursorwidth: 4,
				preventmultitouchscrolling: false,
				horizrailenabled: true,
				cursorborder: "0px solid transparent",
				scrollspeed: 200,
				cursorborderradius: "0"
			});
		}
	});

	accordionWasInited = true;
}

if ($(window).width() > dimensionValue.mobile) {
	accordionInit();
}

$(window).resize(function() {
	if ($(window).width() < dimensionValue.mobile) {
		$(".MapLocations").accordion("destroy");
	} else if ($(window).width() > dimensionValue.mobile) {
		accordionInit();
	}
});

if (currentDevice.type === "desktop") {
	$(document).tooltip({
		items: ".Infras_point",
		classes: {
			"ui-tooltip": "Infras_tipPopup",
			"ui-tooltip-content": "Infras_tipPopupText"
		},
		position: { my: "bottom-25", at: "center" }
	});
}
