class FlatTeaser {
	constructor() {
		let tpl = `
            /*=require ../chunks/FlatTeaser.tpl */
`;
		//trim whitespaces for faster rendering
		tpl = tpl.replace(/\s{2,}/g, "");

		this.compiledTemplate = _.template(tpl);

		this.windowViewsIds = {
			"6": "Пруд",
			"7": "Двор",
			"8": "Лес",
			"9": "Парковка",
			"10": "Пляж"
		};
	}

	renderCards(plansDataArray) {
		let compiledTemplate = this.compiledTemplate;
		let flatsMarkupArray = [];
		plansDataArray.forEach(flatData => {
			flatData.views = _.map(flatData.views, v => {
				return this.windowViewsIds[`${v}`];
			});
			flatsMarkupArray.push(compiledTemplate(flatData));
		});
		return flatsMarkupArray.join("");
	}
}
