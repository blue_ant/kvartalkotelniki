class GeoMap {
	constructor(el, opts) {
		if (!el) {
			console.error('Map class constructor requires "el" argument!');
			return;
		}

		let $el = $(el);

		if (!$el.length) {
			console.error('Map constructor can\'t find given "el" in DOM!');
			return;
		}

		let mapOpts = {
			div: $el.get(0),
			lat: APP_CONFIGS.buildingLatLng.lat + 0.00801968,
			lng: APP_CONFIGS.buildingLatLng.lng - 0.01169263,
			streetViewControl: false,
			fullscreenControl: false,
			mapTypeControl: false,
			zoomControlOptions: {
				position: google.maps.ControlPosition.RIGHT_BOTTOM
			},

			zoom: 14,
			styles: APP_CONFIGS.gmapsStyles
		};

		if (opts) {
			for (let prop in opts) {
				mapOpts[prop] = opts[prop];
			}
		}

		let mapInst = new GMaps(mapOpts);

		mapInst.addMarker({
			position: APP_CONFIGS.buildingLatLng,
			icon: {
				url: "/img/mappins/logo_mark.png",
				scaledSize: new google.maps.Size(74, 75)
			},
			infoWindow: {
				content: APP_CONFIGS.buildingName,
				pixelOffset: new google.maps.Size(-5, 0)
			}
			/*details: {
                type: mrk.type
            }*/
		});

		let roadMarkers = [],
			roadPopups = {};
		let roadPopupOpts = [
			{
				title: "Фото развязки",
				icon: {
					url: "/img/pages/location/road_popup_2.png",
					scaledSize: new google.maps.Size(230, 240)
				},
				zIndex: 9902,
				details: {
					type: 2000
				},
				rendered: false,
				order: 0
			},
			{
				title: "Фото развязки",
				icon: {
					url: "/img/pages/location/road_popup_1.png",
					scaledSize: new google.maps.Size(230, 240)
				},
				zIndex: 9902,
				details: {
					type: 2000
				},
				rendered: false,
				order: 1
			}
		];

		opts.markers.forEach(mrk => {
			let markerOpts = {
				position: mrk.coords,
				title: mrk.title,
				icon: {
					url: mrk.ico,
					scaledSize: new google.maps.Size(44, 46)
				},
				visible: true
			};

			if (!mrk.order) {
				markerOpts.infoWindow = {
					content: `${mrk.title}`,
					pixelOffset: new google.maps.Size(-3, 2)
				};
			}

			if (mrk.order) {
				markerOpts.order = mrk.order;
				roadMarkers.push(mapInst.addMarker(markerOpts)); // Put road markers to object
			} else {
				mapInst.addMarker(markerOpts);
			}

			if (!mrk.order) return;
			roadPopupOpts[mrk.order].lat = mrk.coords.lat;
			roadPopupOpts[mrk.order].lng = mrk.coords.lng;

			roadMarkers.forEach((item, index) => {
				google.maps.event.addListener(item, "click", function() {
					let order = item.order;

					if (roadPopups[`popup${order}`]) {
						for (let item in roadPopups) {
							roadPopups[item].setVisible(false);
						}
						roadPopups[`popup${index}`].setVisible(true);
						return;
					}
					for (let item in roadPopups) {
						roadPopups[item].setVisible(false);
					}
					roadPopups[`popup${order}`] = mapInst.addMarker(roadPopupOpts[order]);

					setTimeout(function() {
						google.maps.event.addListener(roadPopups[`popup${order}`], "click", function() {
							for (let item in roadPopups) {
								roadPopups[item].setVisible(false);
							}
						});
					}, 100);
				});
			});
		});

		this.$el = $el;
		this.gmapsInst = mapInst;
	}

	//TODO: add destroy method
	/*destroy(){

    }*/
}
