class VimeoAPI {
	constructor(el) {}

	pause(el) {
		if (el) {
			if (el instanceof jQuery) {
				throw new Error("Element must not to be a jQuery object");
			}
			let videos = el;

			for (let i = 0; i < videos.length; i++) {
				let video = new Vimeo.Player(videos[i]);
				video.pause();
			}
		}
	}

	play(el) {
		if (el) {
			if (el instanceof jQuery) {
				throw new Error("Element must not to be a jQuery object");
			}
			let videos = el;

			for (let i = 0; i < videos.length; i++) {
				let video = new Vimeo.Player(videos[i]);
				video.play();
			}
		}
	}

	toStart(el) {
		if (el) {
			if (el instanceof jQuery) {
				throw new Error("Element must not to be a jQuery object");
			}
			let videos = el;

			for (let i = 0; i < videos.length; i++) {
				let video = new Vimeo.Player(videos[i]);
				video.setCurrentTime(0);
			}
		}
	}
}
