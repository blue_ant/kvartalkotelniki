class WowzaAPI {
	constructor(element, opts) {
		if (!element) {
			console.error("Slideshow class requires at least one argument!");
		}
		let el = element;

		let options = {
			license: "PLAY1-hWy8c-zkedJ-mEcUX-4MHah-3axfu",
			title: "",
			description: "",
			autoPlay: false,
			volume: "75",
			mute: false,
			loop: false,
			audioOnly: false,
			uiShowQuickRewind: true,
			uiQuickRewindSeconds: "30"
		};

		if (opts) {
			for (let prop in opts) {
				options[prop] = opts[prop];
			}
		}

		WowzaPlayer.create(el, options);

		this.player = WowzaPlayer.get(el);
		// this.player.onLoad(function() {
		//     console.log('created')
		// })
	}

	play() {
		this.player.play();
	}

	pause() {
		this.player.pause();
	}

	finish() {
		this.player.finish();
	}
}
