class PlanCardList {
	constructor() {
		let cardTpl = `
            /*=require ../chunks/PlanCardList.tpl */
`;
		//trim whitespaces for faster rendering
		cardTpl = cardTpl.replace(/\s{2,}/g, "");

		this.compiledTemplate = _.template(cardTpl);
	}

	renderSingleCard(flatPlanData) {
		return this.compiledTemplate(flatPlanData);
	}

	renderCards(plansDataArray) {
		let compiledTemplate = this.compiledTemplate;
		return compiledTemplate({ plans: plansDataArray });
	}
}
