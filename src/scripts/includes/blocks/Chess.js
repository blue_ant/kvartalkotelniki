class Chess {
	constructor() {
		let tplStr = `
            /*=require ../chunks/chess_section.tpl */
`;

		//trim whitespaces for faster rendering
		tplStr = tplStr.replace(/\s{2,}/g, "");

		this.compiledTemplate = _.template(tplStr);
	}

	renderSection(data) {
		return this.compiledTemplate(data);
	}
}
