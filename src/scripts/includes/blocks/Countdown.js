class Countdown {
	constructor(el) {
		let $el = $(el);
		let $clock = $el.find(".Countdown_clock");

		let until = $clock.data("countdown-until");
		//until = new Date().getTime() + 5000;

		$clock
			.on("update.countdown", function(event) {
				let tagWrapRegexp = /(\d)/g;
				let replaceWithStr = '<span class="Countdown_digit">$1</span>';

				let daysMarkup = event.strftime("%D").replace(tagWrapRegexp, replaceWithStr);
				let hoursMarkup = event.strftime("%H").replace(tagWrapRegexp, replaceWithStr);
				let minutesMarkup = event.strftime("%M").replace(tagWrapRegexp, replaceWithStr);

				let markup = `
<div class="Countdown_clockItem">
<div class="Countdown_digitsPair">${daysMarkup}</div>
<div class="Countdown_itemLabel">ДНИ</div>
</div>
<div class="Countdown_clockItem">
<div class="Countdown_digitsPair">${hoursMarkup}</div>
<div class="Countdown_itemLabel">ЧАСЫ</div>
</div>
<div class="Countdown_clockItem">
<div class="Countdown_digitsPair">${minutesMarkup}</div>
<div class="Countdown_itemLabel">МИНУТЫ</div>
</div>
`;
				this.innerHTML = event.strftime(markup);
			})
			.on("finish.countdown", function(event) {
				event.preventDefault();
				$el.remove();
				console.info("Remove countdown element, because the time is up!");
			})
			.countdown(until, {
				precision: 60000
			});
	}
}
