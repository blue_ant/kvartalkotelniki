class Slideshow {
	constructor(el) {
		if (!el) {
			throw new Error("Slideshow class requires at least one argument!");
		}
		let $el = $(el);

		let sliderInstance = new Swiper($el.find(".swiper-container").get(0), {
			speed: 1000,
			loop: true,
			effect: "fade",
			allowTouchMove: false,
			navigation: {
				prevEl: $el.find(".Slideshow_arrow-prev").get(0),
				nextEl: $el.find(".Slideshow_arrow-next").get(0)
			},
			autoplay: {
				delay: 10000,
				disableOnInteraction: false
			}
		});

		let $counter = $el.find(".Slideshow_counter");

		sliderInstance.on("slideChange", () => {
			this.updateSliderElements();
		});

		$el
			.find(".Slideshow_linksList")
			.find('a[href*="#"]')
			.on("click", function(event) {
				event.preventDefault();

				let $target = $(this.hash);

				if ($target.length) {
					let offset = $target.offset().top;
					if ($target.attr("id") === "promosSection") {
						offset += 16;
					}
					$("html, body").animate(
						{
							scrollTop: offset - $(".Header").height() - 16
						},
						1000
					);
				}
			});

		this.$el = $el;
		this.$counter = $counter;
		this.sliderInstance = sliderInstance;

		this.$slideTitle = $el.find(".Slideshow_slideTitle");
		this.$slideDesc = $el.find(".Slideshow_slideDesc");
		this.updateSliderElements();
	}

	updateSliderElements() {
		let sliderInstance = this.sliderInstance;
		let activeIndex = sliderInstance.realIndex + 1;
		this.$slideTitle.html(sliderInstance.slides[activeIndex].dataset.slideTitle);
		this.$slideDesc.html(sliderInstance.slides[activeIndex].dataset.slideDesc);
		this.$counter.html(_.padStart(activeIndex, 2, "0"));
	}

	/*destroy() {
        this.sliderInstance.trigger('destroy.owl.carousel');
    }*/
}
