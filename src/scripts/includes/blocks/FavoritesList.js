class FavoritesList {
	constructor() {
		const STORE_NAME = "favorites_list";

		let favorites = localStorage.getItem(STORE_NAME);

		if (favorites) {
			favorites = JSON.parse(favorites);
		} else {
			favorites = [];
		}

		this.favorites = favorites;
		this.STORE_NAME = STORE_NAME;
	}

	add(id) {
		if (!_.includes(this.favorites, id)) {
			this.favorites.push(id);
			localStorage.setItem(this.STORE_NAME, JSON.stringify(this.favorites));
		}

		console.log("favorites now is: ", this.favorites);
	}

	remove(id) {
		this.favorites = _.without(this.favorites, id);
		localStorage.setItem(this.STORE_NAME, JSON.stringify(this.favorites));

		console.log("favorites now is: ", this.favorites);
	}
}
