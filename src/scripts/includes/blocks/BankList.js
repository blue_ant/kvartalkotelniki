class BankList {
	constructor() {
		let tplStr = `
            /*=require ../chunks/BanksList.tpl */
`;

		//trim whitespaces for faster rendering
		tplStr = tplStr.replace(/\s{2,}/g, "");

		this.compiledTemplate = _.template(tplStr);
	}

	prepareData(data) {
		let opts = data.opts;

		data.banks = _.sortBy(data.banks, "rateFrom");

		for (let i = data.banks.length - 1; i >= 0; i--) {
			let bnkdt = data.banks[i];

			// prepare banks some datas for rendering
			if (!bnkdt.rateTo) bnkdt.rateTo = bnkdt.rateFrom;
			bnkdt.rateAverage = (bnkdt.rateFrom + bnkdt.rateTo) / 2;

			//calculate monthly payment
			{
				let c = opts.price - opts.firstPay; // сумма кредита
				let p = bnkdt.rateAverage; // годовая процентная ставка
				let n = opts.duration * 12; // срок кредита в месяцах
				let a = 1 + p / 1200; // знаменатель прогрессии
				let k = (Math.pow(a, n) * (a - 1)) / (Math.pow(a, n) - 1); // коэффициент ежемесячного платежа
				let sm = k * c; // ежемесячный платёж

				bnkdt.monthlyPayment = parseInt(sm).toLocaleString("ru-RU");
			}
		}

		return data;
	}

	renderList(data) {
		let dataToRender = this.prepareData(data);
		return this.compiledTemplate(dataToRender);
	}
}
