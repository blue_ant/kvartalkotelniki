// TODO: refactor and optimize this class!
class FilterForm {
	constructor(el, opts) {
		if (!el) {
			console.error('FilterForm class constructor requires "el" argument!');
			return;
		}

		restoreFilterFormInputs();

		const submitHandler = _.debounce(opts.submitHandler, 700);

		let $filterForm = $(el);

		if (!$filterForm.length) {
			console.error('FilterForm constructor can\'t find given "el" in DOM!');
			return;
		}
		$("#priceSortSelect").fSelect({
			placeholder: "Сортировать",
			showSearch: false
		});

		$("#viewsSelect").fSelect({
			placeholder: "Вид из окна",
			numDisplayed: 3,
			overflowText: "Вид из окна: {n} выбрано",
			showSearch: false
			//searchText: 'Search',
		});

		let multicheckboxes = [];

		$(".Multycheckbox").each((index, el) => {
			multicheckboxes.push(new Multicheckbox(el));
		});

		let rangeSliders = [];

		rangeSliders.push(
			new RangeSliderFilter($("#floorsRangeSliderCont"), {
				range: true
			})
		);

		rangeSliders.push(
			new RangeSliderFilter($("#areaRangeSliderCont"), {
				range: true
			})
		);

		rangeSliders.push(
			new RangeSliderFilter(
				$("#priceRangeSliderCont"),
				{
					range: true
				},
				"formatted_num"
			)
		);

		rangeSliders.push(
			new RangeSliderFilter(
				$("#finishRangeSliderCont"),
				{
					range: true
				},
				"date_quarters"
			)
		);

		let $inputs = $($filterForm.get(0).elements);

		$inputs.on("change", event => {
			event.preventDefault();
			$filterForm.trigger("submit");
		});

		$filterForm
			.on("submit", event => {
				event.preventDefault();
				submitHandler($filterForm);
				return false;
			})
			.trigger("submit");

		$(".FiltersOpts_resetBtn").on("click", () => {
			this.resetFilterForm();
		});

		this.$filterForm = $filterForm;
		this.$inputs = $inputs;
		this.rangeSliders = rangeSliders;
		this.multicheckboxes = multicheckboxes;
	}

	resetFilterForm() {
		this.$filterForm[0].reset();
		this.rangeSliders.forEach(rangeSliderInst => {
			rangeSliderInst.reset();
		});

		$("#viewsSelect").fSelect("reload");

		this.multicheckboxes.forEach(multiCheckboxInst => {
			multiCheckboxInst.reset();
		});
	}

	/*save() {

    }

    restore() {

    }*/
}
