class SectionsSlider {
	constructor(el) {
		if (!el) {
			throw new Error('SectionsSlider constructor requires "el" argument!');
		}

		let $el = $(el);
		let trackEl = $el.find(".swiper-wrapper").get(0);
		let $sliderArrows = $el.find(".SectionsSlider_arrow");

		let sliderOpts = {
			slidesPerView: "auto",
			spaceBetween: 0,
			navigation: {
				nextEl: $sliderArrows[1],
				prevEl: $sliderArrows[0],
				disabledClass: "SectionsSlider_arrow-disabled"
			},
			centeredSlides: true,
			initialSlide: 2,
			slideActiveClass: "SectionsSlider_slide-active",
			slideNextClass: "SectionsSlider_slide-next",
			slidePrevClass: "SectionsSlider_slide-prev",
			allowTouchMove: false,

			on: {
				setTranslate: function(translate) {
					// fix chrome bluring bug
					trackEl.style.transform = "translate(" + translate + "px, 0)";
				}
			},
			breakpoints: {
				1000: {
					slidesPerView: 1,
					initialSlide: 0,
					autoHeight: true,
					allowTouchMove: true
				}
			}
		};

		this.slider = new Swiper($el, sliderOpts);
		this.$el = $el;
		this.$sliderOpts = sliderOpts;
		$(window).on(
			"resize.SectionsSlider orientationchange.SectionsSlider",
			_.debounce(event => {
				event.preventDefault();
				this.slider.destroy();
				this.slider = new Swiper($el, sliderOpts);
			}, 100)
		);
	}

	setInitialSlide(number) {
		this.slider.destroy();
		this.slider = new Swiper(this.$el, Object.assign(this.$sliderOpts, {initialSlide: number}));
	}

	setSlides(sectionsArr) {
		let freshSlides = [];
		sectionsArr.forEach(sectionSlide => {
			freshSlides.push(`<div class='SectionsSlider_slide swiper-slide'>${sectionSlide}</div>`);
		});

		this.slider.removeAllSlides();
		this.slider.appendSlide(freshSlides);
	}

	destroy() {
		$(window).off(".SectionsSlider");
		this.slider.destroy();
	}
}
