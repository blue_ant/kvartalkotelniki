class Multicheckbox {
	constructor(el) {
		let $el = $(el);
		let $inp = $el.find("input");
		let possibleValues = $inp.data("possible-values");
		$el.on("click", event => {
			event.preventDefault();

			for (let i = possibleValues.length - 1; i >= 0; i--) {
				possibleValues[i] = possibleValues[i] + "";
			}
			let currentVal = $inp.val() + "";
			let currentValIndex = _.indexOf(possibleValues, currentVal);
			let newVal;
			if (currentValIndex === possibleValues.length - 1) {
				newVal = possibleValues[0];
			} else {
				newVal = possibleValues[currentValIndex + 1];
			}
			$inp.val(newVal).trigger("change");
		});

		this.$el = $el;
		this.$inp = $inp;
		this.initialValue = possibleValues[0];
	}

	reset() {
		this.$inp.val(this.initialValue).trigger("change");
	}
}
