window.fbAsyncInit = function() {
	FB.init({
		appId: "2089502644400514",
		autoLogAppEvents: true,
		xfbml: true,
		version: "v2.12"
	});
};

(function(d, s, id) {
	var js,
		fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {
		return;
	}
	js = d.createElement(s);
	js.id = id;
	js.src = "https://connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
})(document, "script", "facebook-jssdk");

function postToFeed(title, desc, url, image) {
	var obj = {
		method: "feed",
		link: url,
		picture: "http://www.url.com/images/" + image,
		name: title,
		description: desc
	};
	function callback(response) {}
	FB.ui(obj, callback);
}

$(".share-facebook").click(function() {
	var elem = $(this);
	postToFeed(elem.data("title"), elem.data("desc"), elem.prop("href"), elem.data("image"));

	return false;
});
