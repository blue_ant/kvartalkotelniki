class AboutProjectSection {
	constructor(el) {
		if (!el) {
			console.error('AboutProjectSection constructor requires "el" argument!');
			return;
		}

		let $el = $(el);

		if (!$el.length) {
			console.error('AboutProjectSection given "el" not found in DOM!');
			return;
		}

		let $swipers = $el.find(".swiper-container");

		let textSlider = new Swiper($swipers.get(0), {
			allowTouchMove: false,
			//autoHeight: true,
			effect: "fade",
			fadeEffect: {
				crossFade: true
			}
		});

		let mirrorSlider = new Swiper($swipers.get(1), {
			pagination: {
				type: "custom",
				el: $el.find(".DotsPager_dotsWrap"),
				renderCustom: (swiper, current, total) => {
					return DotsPager.prototype.renderDots(current, total);
				}
			},
			navigation: {
				nextEl: $el.find(".DotsPager_arrow-right"),
				prevEl: $el.find(".DotsPager_arrow-left"),
				disabledClass: "DotsPager_arrow-disabled"
			}
			//effect: 'fade',
			//allowTouchMove: false,
		});

		mirrorSlider.controller.control = textSlider;

		this.$el = $el;
		this.textSlider = textSlider;
		this.mirrorSlider = mirrorSlider;
	}
	//TODO: add destroy method
	/*destroy(){

    }*/
}
