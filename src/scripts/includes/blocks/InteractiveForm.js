$.validator.addMethod(
	"regex",
	function(value, element, regexp) {
		var re = new RegExp(regexp);
		return this.optional(element) || re.test(value);
	},
	"Поле заполнено неверно"
);

class InteractiveForm {
	constructor(el, opts = {}) {
		this.$form = $(el);
		let $form = this.$form;

		$form.on("submit", function(e) {
			e.preventDefault();
		});

		if ($form.data("action")) {
			$form.prop("action", $form.data("action"));
		}

		$form.find('[name="phone"]').inputmask({
			mask: "+7 999 999-99-99",
			showMaskOnHover: false
		});

		$form.find('[name="captcha"]').inputmask({
			mask: "9999",
			showMaskOnHover: false
		});

		let validatorOpts = {
			rules: {
				phone: {
					required: true,
					regex: /\+7\s\d\d\d\s\d\d\d\-\d\d\-\d\d/
				}
			},
			errorElement: "em",
			// onfocusout: (el, event) => {
			// 	console.log($(el), 'focusot')
			// 	console.log($form)
			// 	$(el).valid();
			// },

			onclick: false,
			focusCleanup: false,
			submitHandler: opts.submitHandler || this.standartFormHandler, //(form)=>{}
			errorPlacement: (/*$errorLabel, $el*/) => {
				return true;
			}
		};

		if (opts.validatorParams) {
			$.extend(true, validatorOpts, opts.validatorParams);
		}

		if (opts.successBlockMod) {
			$.extend(true, opts, { successBlockMod: "default" });
		}

		$form.find('input', 'focusout', function (e) {
			$(e.currentTarget).valid();
		})

		this.opts = opts;
		this.validator = $form.validate(validatorOpts);
		
	}

	standartFormHandler(form) {
		let $form = $(form);

		window.pagePreloader.show();

		let dataToSend = $.extend(true, $form.serializeObject(), {
			Submit: 1,
			url: window.location.href
		});

		$.ajax({
			url: form.action,
			type: form.method,
			data: dataToSend,
			success: function(response) {
				let errorCode = parseInt(response.code);

				if (errorCode === 0) {
					let successText = `<div class="FormSuccess"><div class="FormSuccess__text ">${
						response.success
					}</div></div>`;

					window.requestAnimationFrame(() => {
						$form.hide().after(successText);
					});

					if (form.id === 'ApplicationForm' && $form.data('page-alias') === 'flat-single') {
						var flatId = $('.FlatTypeDescr_head').data('flat-id');
						var flatPrice = parseInt($('.FlatTypeDescr_tblcell-priceVal').text().replace(/\s/g, ''));
						var userEmail = $('.FormModal_inp-email').val();
						var responseId = parseInt(response.id);

						window.criteo_q = window.criteo_q || [];
						var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";

						window.criteo_q.push(
						 { event: "setAccount", account: 56959}, // Это значение остается неизменным
						 { event: "setEmail", email: userEmail }, // Может быть пустой строкой
						 { event: "setSiteType", type: deviceType},
						 { event: "trackTransaction", id: responseId, item: [
						    {id: flatId, price: flatPrice, 
						    quantity: 1 }
						    // добавьте новую запись для каждого продукта в корзине
							]}
						);
					} else if (form.id === 'ApplicationForm' && $form.data('page-alias') === 'flat-type') {
						var userEmail = $('.FormModal_inp-email').val();
						var responseId = parseInt(response.id);

						window.criteo_q = window.criteo_q || [];
						var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";

						window.criteo_q.push(
						 { event: "setAccount", account: 56959}, // Это значение остается неизменным
						 { event: "setEmail", email: userEmail }, // Может быть пустой строкой
						 { event: "setSiteType", type: deviceType},
						 { event: "trackTransaction", id: responseId, item: [
						    {id: 1, price: 1, 
						    quantity: 1 }
						    // добавьте новую запись для каждого продукта в корзине
							]}
						);

						console.log('Test');
					}
				} else {
					alert("Не удалось отправить форму! Попробуйте позже или обратитесть по телефону...");
				}

				window.pagePreloader.hide();
			}
		});
	}

	destroy() {
		this.validator.destroy();
		this.$form.find("input").inputmask("remove");
	}
}
