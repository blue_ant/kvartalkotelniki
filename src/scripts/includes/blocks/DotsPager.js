class DotsPager {
	renderDots(current, total) {
		let paginationMarkup = "";

		for (let i = 1, t = total + 1; i < t; i++) {
			if (i === current) {
				paginationMarkup += `<div class="DotsPager_activeDot">${_.padStart(current, 2, "0")}</div>`;
			} else {
				paginationMarkup += `<div class="DotsPager_dot"></div>`;
			}
		}
		return paginationMarkup;
	}
}
