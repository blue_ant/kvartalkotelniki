<% _.each(plans, function(plan){ %>

<div class="PlanCard" data-plan-id="<%= plan.id %>">
    <div class="PlanCard_inner">
        <div class="PlanCard_controls">
            <div class="PlanCard_ico PlanCard_ico-brush<% if(plan.finish){%> PlanCard_ico-active<%}; %>">
                <svg class="SvgIco" height="15" viewBox="0 0 15 15" width="15" xmlns="http://www.w3.org/2000/svg">
                    <path class="SvgIco_path" d="m4.4 4.91-1.16 1.16c-.1.1-.15.22-.15.36 0 .13.05.26.15.36l1.1 1.1c.21.21.32.5.28.8-.03.3-.19.56-.45.73-3.24 2.06-3.54 2.36-3.64 2.46-.71.71-.71 1.87 0 2.58.72.72 1.87.71 2.59 0 .1-.1.4-.4 2.46-3.64.16-.25.43-.41.72-.45.3-.03.6.07.81.29l1.1 1.1c.2.19.52.19.72 0l1.16-1.16zm-2.04 8.74c-.28.27-.73.27-1.01 0-.28-.28-.28-.74 0-1.01.28-.28.73-.28 1.01 0 .28.27.28.73 0 1.01zm12.49-7.82-4.01 4.02-5.69-5.69 4.01-4.01c.2-.2.52-.2.72 0l .51.51c.15.15.19.38.11.57l-.14.3.28-.13c.2-.1.43-.06.58.09l1.65 1.65c.17.17.19.44.06.64l-1.08 1.6 1.6-1.08c.2-.14.47-.11.64.06l.76.76c.2.2.2.52 0 .71z"></path>
                </svg>
            </div>
            <div class="PlanCard_ico PlanCard_ico-gift<% if(plan.discount || plan.strk){%> PlanCard_ico-active<%};%>">
                <svg class="SvgIco" height="15" viewBox="0 0 13 15" width="13" xmlns="http://www.w3.org/2000/svg">
                    <path class="SvgIco_path" d="m6.97 7.93h5.18v7.07h-5.18zm-6.12 0h5.18v7.07h-5.18zm9.69-4.26c.14-.26.22-1.55.22-1.86 0-1-.81-1.81-1.81-1.81-.74 0-1.49.38-2.07 1.05-.14.16-.27.34-.38.52-.11-.18-.24-.36-.38-.52-.58-.67-1.33-1.05-2.07-1.05-1 0-1.81.81-1.81 1.81 0 .31.08 1.6.22 1.86h-2.46v3.32h6.03v-3.32h.94v3.32h6.03v-3.32zm-7.35-1.86c0-.48.38-.87.86-.87.86 0 1.67 1.88 1.91 2.73h-1.91c-.48 0-.86-1.39-.86-1.86zm5.76 1.86h-1.91c.24-.85 1.05-2.73 1.91-2.73.48 0 .86.39.86.87 0 .47-.38 1.86-.86 1.86z"></path>
                </svg>
            </div>
            <a class="PlanCard_ico PlanCard_ico-star<% if(plan.favorite){%> PlanCard_ico-active<%}; %>" data-flat-type-id="<%= plan.id %>" href="#">
                <svg class="SvgIco SvgIco-star" height="14" viewBox="0 0 15 14" width="15" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path class="SvgIco_path" d="m7.21.09 2.23 4.51 4.99.73-3.61 3.51.85 4.97-4.46-2.35-4.45 2.35.85-4.97-3.61-3.51 4.99-.73z"></path>
                </svg>
            </a>
        </div>
        <a title="Планировка <%= plan.name %>" class="PlanCard_schemeWrap" href="<%= plan.href %>"><img class="PlanCard_scheme1" src="<%= plan.plan %>" alt="">
          <div class="PlanCard_scheme2" style="background-image:url('<%= plan.plan %>');"></div></a>
        <div class="PlanCard_details">
            <div class="PlanCard_price">
                <div class="PlanCard_priceTitle">цена от:</div>
                <div class="PlanCard_priceVals">
                    <div class="PlanCard_priceText PlanCard_priceText-fix"><%= plan.priceMin.toLocaleString('ru-RU') %> ₽</div>
                </div>
            </div>
            <div class="PlanCard_area">
                <div class="PlanCard_areaTitle">площадь oт:</div>
                <div class="PlanCard_areaValue"><%= plan.areaMin %> м²</div>
            </div>
        </div>
    </div>
</div>

<% }); %>

<% if(plans.length === 0) { %>

<div class="PlanCard PlanCard-placehold"><div class="PlanCard_inner"><div class="PlanCard_controls"><div class="PlanCard_ico PlanCard_ico-brush"><svg class="SvgIco" height="15" viewBox="0 0 15 15" width="15" xmlns="http://www.w3.org/2000/svg"><path class="SvgIco_path" d="m4.4 4.91-1.16 1.16c-.1.1-.15.22-.15.36 0 .13.05.26.15.36l1.1 1.1c.21.21.32.5.28.8-.03.3-.19.56-.45.73-3.24 2.06-3.54 2.36-3.64 2.46-.71.71-.71 1.87 0 2.58.72.72 1.87.71 2.59 0 .1-.1.4-.4 2.46-3.64.16-.25.43-.41.72-.45.3-.03.6.07.81.29l1.1 1.1c.2.19.52.19.72 0l1.16-1.16zm-2.04 8.74c-.28.27-.73.27-1.01 0-.28-.28-.28-.74 0-1.01.28-.28.73-.28 1.01 0 .28.27.28.73 0 1.01zm12.49-7.82-4.01 4.02-5.69-5.69 4.01-4.01c.2-.2.52-.2.72 0l .51.51c.15.15.19.38.11.57l-.14.3.28-.13c.2-.1.43-.06.58.09l1.65 1.65c.17.17.19.44.06.64l-1.08 1.6 1.6-1.08c.2-.14.47-.11.64.06l.76.76c.2.2.2.52 0 .71z"></path></svg></div><div class="PlanCard_ico PlanCard_ico-gift"><svg class="SvgIco" height="15" viewBox="0 0 13 15" width="13" xmlns="http://www.w3.org/2000/svg"><path class="SvgIco_path" d="m6.97 7.93h5.18v7.07h-5.18zm-6.12 0h5.18v7.07h-5.18zm9.69-4.26c.14-.26.22-1.55.22-1.86 0-1-.81-1.81-1.81-1.81-.74 0-1.49.38-2.07 1.05-.14.16-.27.34-.38.52-.11-.18-.24-.36-.38-.52-.58-.67-1.33-1.05-2.07-1.05-1 0-1.81.81-1.81 1.81 0 .31.08 1.6.22 1.86h-2.46v3.32h6.03v-3.32h.94v3.32h6.03v-3.32zm-7.35-1.86c0-.48.38-.87.86-.87.86 0 1.67 1.88 1.91 2.73h-1.91c-.48 0-.86-1.39-.86-1.86zm5.76 1.86h-1.91c.24-.85 1.05-2.73 1.91-2.73.48 0 .86.39.86.87 0 .47-.38 1.86-.86 1.86z"></path></svg></div><a class="PlanCard_ico PlanCard_ico-star" data-flat-type-id="1799" href="#"><svg class="SvgIco SvgIco-star" height="14" viewBox="0 0 15 14" width="15" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path class="SvgIco_path" d="m7.21.09 2.23 4.51 4.99.73-3.61 3.51.85 4.97-4.46-2.35-4.45 2.35.85-4.97-3.61-3.51 4.99-.73z"></path></svg></a></div><span class="PlanCard_schemeWrap"><img class="PlanCard_scheme1" src="/img/plan_placeholder.svg" alt=""><div class="PlanCard_scheme2" style="background-image:url('/img/plan_placeholder.svg');"></div></a><div class="PlanCard_details"><div class="PlanCard_price"><div class="PlanCard_priceTitle">цена от:</div><div class="PlanCard_priceVals"><div class="PlanCard_priceText PlanCard_priceText-fix">0&nbsp;000&nbsp;000 ₽</div></div></div><div class="PlanCard_area"><div class="PlanCard_areaTitle">площадь oт:</div><div class="PlanCard_areaValue">00 м²</div></div></div></div></div>

<% }; %>