$.extend($.fancybox.defaults, {
	hash: false,
	infobar: true,
	buttons: ["close"],
	margin: [40, 40],
	lang: "ru",
	transitionEffect: "slide",
	touch: false,
	animationDuration: 200,
	mobile: {
		clickContent: false,
		clickSlide: false
	},
	i18n: {
		ru: {
			CLOSE: "Закрыть",
			NEXT: "Далее",
			PREV: "Назад",
			ERROR: "Не удалось открыть галерею - попробуйте позже",
			PLAY_START: "Запустить слайдшоу",
			PLAY_STOP: "Пауза",
			FULL_SCREEN: "На весь экран",
			THUMBS: "Миниатуры"
		}
	},
	btnTpl: {
		smallBtn:
			'<button data-fancybox-close class="Btn Btn-modalClose" title="{{CLOSE}}"><svg class="SvgIco" height="13" viewBox="0 0 13 13" width="13" xmlns="http://www.w3.org/2000/svg"><path class="SvgIco_path" d="m532.31 19.31h7v2h-7v7h-2v-7h-7v-2h7v-7h2z"  fill-rule="evenodd" transform="matrix(.70710678 .70710678 -.70710678 .70710678 -355.02 -383.75)"/></svg></button>'
	},

	afterClose: function() {
		if (this.type === "inline") {
			let $form = this.$content.find("form:first");
			let validator = $form.trigger("reset").data("validator");
			if (validator) {
				validator.resetForm();
				$form.find("input.error").removeClass("error");
			}

			let $successText = this.$content.find(".FormSuccess");
			if ($successText.length) {
				$form.show();
				$successText.remove();
			}
		}
	}
});
