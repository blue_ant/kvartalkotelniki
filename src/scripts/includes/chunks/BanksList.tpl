<div class="BanksList">
    <% _.forEach(banks, function(bank){ %>
        <div class="BanksList_row">
            <div class="BanksList_cell BanksList_cell-img"><img src="<%= bank.logo %>" title="<%= bank.name %>" alt="ЛОГО <%= bank.name %>"></div>
            <div class="BanksList_cell BanksList_cell-description">
                <h4 class="BanksList_header"><span class="hidden-sm">Первоначальный </span>взнос</h4>
                <p class="BanksList_text">
                    <% if(bank.firstFrom === bank.firstTo) {%>
                        <%= bank.firstFrom %>
                    <%}else if(bank.firstTo === 100){%>
                        от <%= bank.firstFrom %>
                    <%}else{%>
                        <%= bank.firstFrom %> - <%= bank.firstTo %>
                    <%};%>%
                </p>
            </div>
            <div class="BanksList_cell BanksList_cell-rate">
                    <% if(bank.isHinted) {%>
                        <h4 class="BanksList_header BanksList_header-star">Ставка</h4>
                    <%}else{%>
                        <h4 class="BanksList_header">Ставка</h4>
                    <%};%>
                <p class="BanksList_text" style="text-align:center;">
                    от&nbsp;
                    <% if(bank.rateFrom === bank.rateTo) {%>
                        <%= bank.rateFrom %>
                    <%}else{%>
                        <%= bank.rateFrom %> - <%= bank.rateTo %>
                    <%};%>%
            </p>
            </div>
            <div class="BanksList_cell">
                <h4 class="BanksList_header">Срок кредита до</h4>
                <p class="BanksList_text"><%= bank.yearsTo %> лет</p>
            </div>
            <div class="BanksList_cell">
                <h4 class="BanksList_header">Платежи в месяц</h4>
                <p class="BanksList_text"><%= bank.monthlyPayment %> ₽</p>
            </div>
        </div>
        <%}); %>
        <p class="BanksList_tipText"><span class="BanksList_tipSign">*</span>подробности предоставления данных условий уточняйте у менеджера по ипотеке.</p>
</div>