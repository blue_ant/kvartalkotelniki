function GalleryMobileTabs(sliderEl) {
	let $el = $(".GalleryTabs_item");

	$el.click(function() {
		$el.removeClass("GalleryTabs_item-active");
		$(this).addClass("GalleryTabs_item-active");

		let tabPackId = $(this).data("trigger-pack");

		$(`.GallerySidebar_item[data-trigger-pack="${tabPackId}"]`)
			.find(".GallerySidebar_linkArea")
			.click();
	});

	$(".GallerySidebar_img").click(function() {
		let triggerBlockId = $(this)
			.closest(".GallerySidebar_item")
			.data("trigger-pack");
		$(".GalleryTabs_item").removeClass("GalleryTabs_item-active");
		$(`.GalleryTabs_item[data-trigger-pack="${triggerBlockId}"]`).addClass("GalleryTabs_item-active");
	});
}
