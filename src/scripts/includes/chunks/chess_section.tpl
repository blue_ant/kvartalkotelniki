<%
    let floorFlatsQuantity = _.maxBy(floors, function(flr) { return flr.flats.length }).flats.length;
%>
<div class="Chess">
    <div class="Chess_roof Chess_roof-spanto<%=floorFlatsQuantity%>"></div>
    <div class="Chess_inner">
        <% _.forEachRight(floors, function(floor){ %>
        <% let isLowFloor = floor.floorNum < 7; %>
        <div class="Chess_row<% if(isLowFloor){%> Chess_row-lowFloor<%}; %>">
            <% _.forEach(floor.flats, function(flat){%>
                <% if(flat.strk){flat.discount = true;} %>
                <% if(flat.status === 'sale'){%>
                    <div class="Chess_cell Chess_cell-1of<%=floorFlatsQuantity%><% if(isLowFloor){%> Chess_cell-onLowFloor<%}; %>">
                        <a class="Chess_flatlink Chess_flatlink-rooms<%=flat.rooms%> <% if(flat.discount){%>Chess_flatlink-discount<%}; %> <% if(flat["finish-full"]){%>Chess_flatlink-finish-full<%}; %> <% if(flat["finish-white-box"]){%>Chess_flatlink-finish-whitebox<%}; %> Chess_flatlink-active" href="<%=flat.href%>">
                            <template>
                                <div class="PopupInfo">
                                    <div class="PopupInfo_head PopupInfo_head-v<%=flat.rooms%><% if(flat.discount){%> PopupInfo_head-discount<%}; %>">
                                        <span>Квартира</span>
                                        <span><%=flat.num%></span>
                                    </div>
                                    <dl class="PopupInfo_list">
                                        <dt class="PopupInfo_dt">Площадь</dt>
                                        <dd class="PopupInfo_dd"><%=flat.area%> м²</dd>
                                        <dt class="PopupInfo_dt">Спален</dt>
                                        <dd class="PopupInfo_dd"><%=flat.rooms || 'студия'%></dd>

                                        <dt class="PopupInfo_dt">Цена</dt>
                                        <dd class="PopupInfo_dd"><%=flat.price.toLocaleString('ru-RU')%> ₽</dd>

                                        <dt class="PopupInfo_dt">Отделка</dt>
                                        <dd class="PopupInfo_dd">
                                            <% if(flat["finish-full"] || flat["finish-white-box"]){%>
                                            Доступна
                                            <%} else{ %>
                                            Нет
                                            <%}; %>
                                        </dd>
                                    </dl>
                                </div>
                            </template>
                        </a>
                    </div>
                <%} else{ %>
                    <div class="Chess_cell Chess_cell-1of<%=floorFlatsQuantity%><%=floorFlatsQuantity%><% if(isLowFloor){%> Chess_cell-onLowFloor<%}; %>"></div>
                <%}; %>
            <%}); %>

            <div class="Chess_rowNum"><%=_.padStart(floor.floorNum, 2, 0)%></div>
        </div>
        <%}); %>

        <div class="Chess_row Chess_row-footer">

            <% let firstFloorFlatQuantity = floors[0].flats.length; %>
            <% for(i = 0; i < firstFloorFlatQuantity; i++){%>
            <div class="Chess_cell Chess_cell-1of<%=firstFloorFlatQuantity%> Chess_cell-footer Chess_cell-onLowFloor"></div>
            <%}; %>
        </div>
        <div class="Chess_buildingNumbers">
            <div class="Chess_bldNumsItem Chess_bldNumsItem-n1">
                <div class="Chess_bldNumLabel">Корпус</div>
                <div class="Chess_bldNumVal Chess_bldNumVal-houseNum"></div>
            </div>
            <div class="Chess_bldNumsItem Chess_bldNumsItem-n2">
                <div class="Chess_bldNumLabel">Секция</div>
                <div class="Chess_bldNumVal"><%=sectionNum%></div>
            </div>
        </div>
    </div>
</div>
