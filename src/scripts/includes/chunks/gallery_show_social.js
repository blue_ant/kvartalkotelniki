function showSocial(trigger, socialBlock, className) {
	if (!className) {
		console.log("Specify class!");
	}

	let classToApply;

	if (classToApply) {
		classToApply = className;
	} else {
		classToApply = "GalleryShow_socialLinks-active";
	}

	trigger.click(function(e) {
		e.stopPropagation();
		socialBlock.addClass("GalleryShow_socialLinks-active");
	});
	$(document).click(function(e) {
		socialBlock.removeClass("GalleryShow_socialLinks-active");
	});
}
