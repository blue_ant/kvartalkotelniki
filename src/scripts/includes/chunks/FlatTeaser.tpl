<%
price = price.toLocaleString("ru-RU");
if(strk) discount = true;
if(!priceOld) var priceOld = price;
%>
<a class="FlatTeaser" href="<%= href %>">
    <div class="FlatTeaser_inner">
        <div class="FlatTeaser_detailsGroup FlatTeaser_detailsGroup-n1">
            <div class="FlatTeaser_detail FlatTeaser_detail-n1">
                <div class="FlatTeaser_lbl">Квартира</div>
                <div class="FlatTeaser_val">№<%= num %></div>
            </div>
            <div class="FlatTeaser_detail FlatTeaser_detail-n2">
                <div class="FlatTeaser_lbl">Корпус</div>
                <div class="FlatTeaser_val"><%= house %></div>
            </div>
            <div class="FlatTeaser_detail FlatTeaser_detail-n3">
                <div class="FlatTeaser_lbl">Вид из окна</div>
                <div class="FlatTeaser_val">
                  <% if(views.length){ %>
                    <%= viewsStr.join(", ") %>
                  <%}else{%>
                    -
                  <%};%></div>
            </div>
        </div>
        <div class="FlatTeaser_detailsGroup FlatTeaser_detailsGroup-n2">
            <div class="FlatTeaser_detail FlatTeaser_detail-n4">
                <div class="FlatTeaser_lbl">Площадь</div>
                <div class="FlatTeaser_val"><%= area %> м²</div>
            </div>
            <div class="FlatTeaser_detail FlatTeaser_detail-n5">
                <div class="FlatTeaser_lbl">Этаж</div>
                <div class="FlatTeaser_val"><%= floor %></div>
            </div>
            <div class="FlatTeaser_detail FlatTeaser_detail-n6">
                <div class="FlatTeaser_lbl">Стоимость</div>
                <div class="FlatTeaser_val<% if(discount){ %> FlatTeaser_val-highlight<%};%>"><%= price %> ₽ </div>
            </div>
        </div>
    </div>
    <div class="FlatTeaser_controlsWrap">
        <div class="FlatTeaser_controlItem<% if(finish){ %> FlatTeaser_controlItem-active<%};%>">
            <svg class="SvgIco" height="15" viewBox="0 0 15 15" width="15" xmlns="http://www.w3.org/2000/svg">
                <path class="SvgIco_path" d="m4.4 4.91-1.16 1.16c-.1.1-.15.22-.15.36 0 .13.05.26.15.36l1.1 1.1c.21.21.32.5.28.8-.03.3-.19.56-.45.73-3.24 2.06-3.54 2.36-3.64 2.46-.71.71-.71 1.87 0 2.58.72.72 1.87.71 2.59 0 .1-.1.4-.4 2.46-3.64.16-.25.43-.41.72-.45.3-.03.6.07.81.29l1.1 1.1c.2.19.52.19.72 0l1.16-1.16zm-2.04 8.74c-.28.27-.73.27-1.01 0-.28-.28-.28-.74 0-1.01.28-.28.73-.28 1.01 0 .28.27.28.73 0 1.01zm12.49-7.82-4.01 4.02-5.69-5.69 4.01-4.01c.2-.2.52-.2.72 0l .51.51c.15.15.19.38.11.57l-.14.3.28-.13c.2-.1.43-.06.58.09l1.65 1.65c.17.17.19.44.06.64l-1.08 1.6 1.6-1.08c.2-.14.47-.11.64.06l.76.76c.2.2.2.52 0 .71z" transform=""></path>
            </svg>
        </div>
        <div class="FlatTeaser_controlItem<% if(discount){ %> FlatTeaser_controlItem-highlight<%};%>">
            <svg class="SvgIco" height="15" viewBox="0 0 13 15" width="13" xmlns="http://www.w3.org/2000/svg">
                <path class="SvgIco_path" d="m6.97 7.93h5.18v7.07h-5.18zm-6.12 0h5.18v7.07h-5.18zm9.69-4.26c.14-.26.22-1.55.22-1.86 0-1-.81-1.81-1.81-1.81-.74 0-1.49.38-2.07 1.05-.14.16-.27.34-.38.52-.11-.18-.24-.36-.38-.52-.58-.67-1.33-1.05-2.07-1.05-1 0-1.81.81-1.81 1.81 0 .31.08 1.6.22 1.86h-2.46v3.32h6.03v-3.32h.94v3.32h6.03v-3.32zm-7.35-1.86c0-.48.38-.87.86-.87.86 0 1.67 1.88 1.91 2.73h-1.91c-.48 0-.86-1.39-.86-1.86zm5.76 1.86h-1.91c.24-.85 1.05-2.73 1.91-2.73.48 0 .86.39.86.87 0 .47-.38 1.86-.86 1.86z" transform=""></path>
            </svg>
        </div>
    </div>
    <div class="FlatTeaser_pricesWrap">
      <% if(discount){ %>
        <div class="FlatTeaser_prices FlatTeaser_prices-highlight">
            <div class="FlatTeaser_lbl">Стоимость</div>
            <%if(strk){ %>
                <div class="FlatTeaser_price FlatTeaser_price-old"><%= price.toLocaleString("ru-RU") %> ₽</div>
            <%}else{%>
                <ins class="FlatTeaser_price FlatTeaser_price-new"><%= price %> ₽</ins>
                <del class="FlatTeaser_price FlatTeaser_price-old"><%= priceOld.toLocaleString("ru-RU") %> ₽</del>
            <%};%>
        </div>
      <%}else{%>
         <div class="FlatTeaser_prices">
            <div class="FlatTeaser_lbl">Стоимость</div>
            <div class="FlatTeaser_price"><%= price %> ₽</div>
        </div>
      <%};%>
    </div>
</a>