if (currentDevice.type === "desktop") {
	let $sectionChooseBlock = $(".SectChoose");
	let $svgScheme = $sectionChooseBlock.find(".SectChoose_plan");
	let $linksToFlats = $svgScheme.find(".SectChoose_link");
	let $popupContents = $sectionChooseBlock.find(".SectChoose_popupContent");
	$svgScheme.tooltip({
		items: ".SectChoose_link",
		hide: 150,
		show: 150,
		track: true,
		position: { my: "left+15 top+15" },
		content: function() {
			let index = $linksToFlats.index(this);
			return $popupContents.eq(index).html();
		}
	});
}
