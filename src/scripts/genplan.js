let genplanFitblock = new Fitblock($(".Fitblock-genplan").get(0), {
	centerOffset_xs: 1.4
});

if (currentDevice.type === "desktop") {
	genplanFitblock.$el.tooltip({
		items: ".Genplan_marker-active",
		content: function() {
			return this.getElementsByTagName("template")[0].innerHTML;
		}
	});
}
