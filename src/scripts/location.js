let routesSource = [
	{
		origin: [55.709236, 37.734399],
		destination: [APP_CONFIGS.buildingLatLng.lat, APP_CONFIGS.buildingLatLng.lng],
		travelMode: "driving",
		strokeColor: "#6F9EFC",
		waypoints: [
			{
				location: {
					lat: 55.653355,
					lng: 37.860234
				}
			}
		],
		strokeOpacity: 1,
		strokeWeight: 4,
		id: 0
	},
	{
		origin: [55.623382, 37.793493],
		destination: [APP_CONFIGS.buildingLatLng.lat, APP_CONFIGS.buildingLatLng.lng],
		travelMode: "driving",
		strokeColor: "#AC6CFC",
		strokeOpacity: 1,
		strokeWeight: 4,
		id: 1
	},
	{
		origin: [55.705387, 37.833459],
		destination: [APP_CONFIGS.buildingLatLng.lat, APP_CONFIGS.buildingLatLng.lng],
		travelMode: "driving",
		strokeColor: "#FC896F",
		waypoints: [
			{
				location: {
					lat: 55.667074,
					lng: 37.837932
				}
			}
		],
		strokeOpacity: 1,
		strokeWeight: 4,
		id: 2
	},
	{
		origin: [55.674529, 37.859296],
		destination: [APP_CONFIGS.buildingLatLng.lat, APP_CONFIGS.buildingLatLng.lng],
		travelMode: "TRANSIT",
		strokeColor: "#52a88b",
		strokeOpacity: 1,
		strokeWeight: 4,
		id: 3
	},
	{
		origin: [55.682331, 37.896656],
		destination: [APP_CONFIGS.buildingLatLng.lat, APP_CONFIGS.buildingLatLng.lng],
		travelMode: "TRANSIT",
		strokeColor: "#ff747c",
		strokeOpacity: 1,
		strokeWeight: 4,
		id: 4
	},
	{
		origin: [55.632869, 37.765936],
		destination: [APP_CONFIGS.buildingLatLng.lat, APP_CONFIGS.buildingLatLng.lng],
		travelMode: "TRANSIT",
		strokeColor: "#186189",
		strokeOpacity: 1,
		strokeWeight: 4,
		id: 5
	},
	{
		origin: [55.6764475, 37.7620763],
		destination: [APP_CONFIGS.buildingLatLng.lat, APP_CONFIGS.buildingLatLng.lng],
		travelMode: "TRANSIT",
		strokeColor: "#a060df",
		strokeOpacity: 1,
		strokeWeight: 4,
		id: 6
	}
];


/* Yandex Map */
let yandexMap;
let yandexMarkers = [
	{
		coords: [55.639524, 37.819216],
		title: "Развязка",
		balloonOffset: [23, -24],
		icon: {
			url: "/img/mappins/road.png",
			size: [45, 45]
		}
	},
	{
		coords: [55.656952, 37.839116],
		title: "Развязка",
		balloonOffset: [23, -24],
		icon: {
			url: "/img/mappins/road.png",
			size: [45, 45]
		}
	},
	{
		coords: [55.68722, 37.830425],
		title: "Развязка",
		balloonOffset: [9, -24],
		icon: {
			url: "/img/mappins/road.png",
			size: [45, 45]
		}
	},
	{
		coords: [55.638661, 37.845971],
		title: "ЖК «Новые Котельники»",
		balloonOffset: [9, -24],
		icon: {
			url: "/img/mappins/logo_mark.png",
			size: [74, 75]
		}
	},
	{
		coords: [55.67372, 37.859038],
		title: "метро «Котельники»",
		balloonOffset: [9, -24],
		icon: {
			url: "/img/mappins/metro.png",
			size: [45, 45]
		}
	},
	{
		coords: [55.632815, 37.766049],
		title: "метро «Алма-Атинская»",
		balloonOffset: [9, -24],
		icon: {
			url: "/img/mappins/metro.png",
			size: [45, 45]
		}
	},
	{
		coords: [55.6764475, 37.7620763],
		title: "метро «Люблино»",
		balloonOffset: [9, -24],
		icon: {
			url: "/img/mappins/metro.png",
			size: [45, 45]
		}
	}
];

let routesCollections = [];

ymaps.ready(init);
function init() { 
  yandexMap = new ymaps.Map("Map", {
    center: [55.653465, 37.823784],
    zoom: 13,
  });

  let markerCollection, multiRoute;

  function drawMarkers (yandexMarkers) {
		// Создание макета балуна
	  let MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
	    '<div class="Balloon Balloon-top">' +
	    '<a class="Balloon_close" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/><path d="M0 0h24v24H0z" fill="none"/></svg></a>' +
	    '<div class="Balloon_arrow"></div>' +
	    '<div class="Balloon_inner">' +
	    '$[[options.contentLayout observeSize maxWidth=235 maxHeight=350]]' +
	    '</div>' +
	    '</div>', {
	    build: function () {
	      this.constructor.superclass.build.call(this);
	      this._$element = $('.Balloon', this.getParentElement());
	      this.applyElementOffset();
	      this._$element.find('.Balloon_close').on('click', $.proxy(this.onCloseClick, this));
	    },

	    clear: function () {
	      this._$element.find('.Balloon_close').off('click');
	      this.constructor.superclass.clear.call(this);
	    },

	    onSublayoutSizeChange: function () {
	      MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);
	      if(!this._isElement(this._$element)) {
	      	return;
	      }
	      this.applyElementOffset();
	      this.events.fire('shapechange');
	    },

	    applyElementOffset: function () {
	      this._$element.css({
	        left: -(this._$element[0].offsetWidth / 2),
	        top: -(this._$element[0].offsetHeight + this._$element.find('.Balloon_arrow')[0].offsetHeight)
	      });
	    },

	    onCloseClick: function (e) {
	      e.preventDefault();
	      this.events.fire('userclose');
	    },

	    getShape: function () {
	      if(!this._isElement(this._$element)) {
	        return MyBalloonLayout.superclass.getShape.call(this);
	      }

	      var position = this._$element.position();

	      return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
	        [position.left, position.top], [
	          position.left + this._$element[0].offsetWidth,
	          position.top + this._$element[0].offsetHeight + this._$element.find('.Balloon_arrow')[0].offsetHeight
	        ]
	      ]));
	    },

	    _isElement: function (element) {
	      return element && element[0] && element.find('.Balloon_arrow')[0];
	    }
	  });

		// Создание вложенного макета содержимого балуна.
	  let MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
	    '<h3 class="Balloon_title">$[properties.balloonHeader]</h3>' +
	    '<div class="Balloon_content">$[properties.balloonContent]</div>'
	  );

		markerCollection = new ymaps.GeoObjectCollection({}, {
			iconLayout: 'default#image',
			hideIconOnBalloonOpen: false,
		});

		yandexMarkers.forEach(item => {
	    markerCollection.add(new ymaps.Placemark(item.coords, {
	    	hintContent: item.title,
	    	balloonContent: item.title,
	    }, {
	    	iconImageHref: item.icon.url,
	    	iconImageSize: item.icon.size,
	    	balloonOffset: item.balloonOffset,
	    	balloonLayout: MyBalloonLayout,
	    	balloonContentLayout: MyBalloonContentLayout,
	    }));
		});

		yandexMap.geoObjects.add(markerCollection);
	}

	drawMarkers(yandexMarkers);

	yandexMap.events.add('click', function(e) {
    let coords = e.get('coords');

    let clickPosition = {
    	lat: coords[0],
    	lng: coords[1]
    };

    markerCollection.removeAll();
    yandexMap.geoObjects.remove(multiRoute);
    drawMarkers(yandexMarkers);

    markerCollection.add(new ymaps.Placemark([clickPosition.lat, clickPosition.lng], {
    	hintContent: "Точка отправления"
    }, {
      iconLayout: 'default#image',
    	iconImageHref: "/img/mappins/dest-icon.svg",
    	iconImageSize: [18, 18]
    }));

    markerCollection.add(new ymaps.Placemark([55.633123, 37.851133], {
    	hintContent: "Точка назначения ЖК Котельники"
    }, {
      iconLayout: 'default#image',
    	iconImageHref: "/img/mappins/dest-pulse-icon.svg",
    	iconImageSize: [90, 90]
    }));

    // Построение маршрута.
		multiRoute = new ymaps.multiRouter.MultiRoute({   
	    referencePoints: [
        [clickPosition.lat, clickPosition.lng],
        [55.638434, 37.857576],
	    ]
		}, {
	      routeActiveStrokeWidth: 4,
	      routeActiveStrokeColor: "#90CEAA"
		});

		// Добавление маршрута на карту.
		yandexMap.geoObjects.add(multiRoute);
	});

  yandexMap.controls.remove('geolocationControl');
  yandexMap.controls.remove('searchControl');
  yandexMap.controls.remove('trafficControl');
  yandexMap.controls.remove('typeSelector');
  yandexMap.controls.remove('fullscreenControl');
  yandexMap.controls.remove('rulerControl');
  yandexMap.behaviors.disable(['scrollZoom']);
}

$("[data-route-id]").click(function() {
	$(this).toggleClass("isOff");
	cleanRoutes(routesCollections);
	drawCarRoutes($("[data-route-id]"), routesSource);
});

$(".MapLocations").accordion({
	header: "h3",
	active: false,
	heightStyle: "content",
	collapsible: true,
	animate: 300,
	create: function(/*event, ui*/) {
		for (let i = 0; i < $(".MapLocations .MapLocations_header").length; i++) {
			$(`.MapLocations .MapLocations_header:eq(${i})`).append($(".MapLocations .MapLocations_headerIcon").get(i));
		}
		$(".MapLocations_wrapper").niceScroll({
			cursorcolor: "#A7A8AA",
			cursorfixedheight: 70,
			cursoropacitymin: 1,
			cursorwidth: 4,
			preventmultitouchscrolling: false,
			horizrailenabled: true,
			cursorborder: "0px solid transparent",
			scrollspeed: 200,
			cursorborderradius: "0"
		});
		$("[data-route-id]").addClass("isOff");
	},
	activate: (event, ui) => {
		if (Object.keys(ui.newPanel).length === 0) {
			$("[data-route-id]").addClass("isOff");
			return;
		}
		if (!ui.oldPanel) {
			$(ui.newPanel)
				.find("[data-route-id]")
				.removeClass("isOff");
			// map.cleanRoute();
			cleanRoutes(routesCollections);
			setTimeout(drawCarRoutes($("[data-route-id]"), routesSource), 0);
			return;
		}

		$(ui.oldPanel)
			.find("[data-route-id]")
			.addClass("isOff");
		$(ui.newPanel)
			.find("[data-route-id]")
			.removeClass("isOff");

		// map.cleanRoute();
		cleanRoutes(routesCollections);
		setTimeout(drawCarRoutes($("[data-route-id]"), routesSource), 0);
	}
});

new Swiper(".MapSlider", {
	slidesPerView: "auto",
	centeredSlides: true,
	spaceBetween: 15,
	on: {
		slideChangeTransitionStart: function() {
			let currentSlide = this.slides[this.realIndex];

			$(".MapSlider [data-route-id]").addClass("isOff");
			$(currentSlide)
				.find("[data-route-id]")
				.removeClass("isOff");

			// map.cleanRoute();
			cleanRoutes(routesCollections);
			setTimeout(drawCarRoutes($("[data-route-id]"), routesSource), 0);
		}
	}
});

$(".MapTip_close").click(function() {
	$(this)
		.parent(".MapTip")
		.addClass("MapTip-hidden");
});


function renderRoutes(routesArray) {

	routesArray.forEach(route => {
		var multiRoute = new ymaps.multiRouter.MultiRoute({
			// Описание опорных точек мультимаршрута.
			referencePoints: [
					route.origin,
					route.destination
			],
			// Параметры маршрутизации.
			params: {
					// Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
					results: 2
			}
		}, {
				// Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
				boundsAutoApply: false,
				routeStrokeColor: route.strokeColor,
				routeActiveStrokeColor: route.strokeColor,
				pinIconFillColor: route.strokeColor,
		});
		routesCollections.push(multiRoute);
		yandexMap.geoObjects.add(multiRoute);

	});
};

function cleanRoutes(routes) {
	$(routes).each((i, e) => {
		yandexMap.geoObjects.remove(e);
	}) 
}

function drawCarRoutes(triggers, routes) {
	let triggerHeaders = triggers;

	triggerHeaders.each((index, item) => {
		let routeArr = routes.filter(routeEl => {
			if ($(item).data("route-id") === routeEl.id) return routeEl;
		});

		if (!$(item).hasClass("isOff")) {
			renderRoutes(routeArr);
		}
	});
}

// START: draw buildings
{
	let bldsCoordinates = [
		[
			[55.638497, 37.847797],
			[55.638707, 37.847546],
			[55.638805, 37.847569],
			[55.63906, 37.848239],
			[55.639194, 37.84809],
			[55.638907, 37.847318],
			[55.638692, 37.84727],
			[55.638422, 37.847582]
		],
		[
			[55.639145, 37.848335],
			[55.639291, 37.848164],
			[55.639716, 37.849269],
			[55.639569, 37.849442],
			[55.639149, 37.848337]
		],
		[
			[55.639634, 37.849616],
			[55.639793, 37.849423],
			[55.640098, 37.850219],
			[55.639564, 37.850858],
			[55.639463, 37.850592],
			[55.639831, 37.850144]
		]
	];
	// let polygon = bldsCoordinates.forEach(item => {
	// 	map.drawPolygon({
	// 		paths: item,
	// 		strokeColor: "#bdc9c9",
	// 		strokeOpacity: 1,
	// 		strokeWeight: 0,
	// 		fillColor: "#bdc9c9",
	// 		fillOpacity: 1
	// 		//editable: true,
	// 	});
	// });
}
// END: draw buildings
