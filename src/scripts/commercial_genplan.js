let genplanFitblock = new Fitblock($(".Fitblock-genplan2d").get(0), {
	centerOffset_xs: 0
});

let $numbers = genplanFitblock.$el.find(".Genplan_smallNumber");
let $bldShapes = genplanFitblock.$el.find(".BldChoose_bld");

$bldShapes.hover(
	function() {
		let index = $bldShapes.index(this);
		$numbers.eq(index).addClass("Genplan_smallNumber-active");
	},
	function() {
		let index = $bldShapes.index(this);
		$numbers.eq(index).removeClass("Genplan_smallNumber-active");
	}
);
