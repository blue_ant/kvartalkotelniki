// START: about features section sliders
let featuresMirrorSlideshow = new AboutProjectSection("#featuresPageMirrorSliderSection");
// END: about features section sliders

// START: infra map

let yandexMap;
let yandexMarkers = window.ifrasObjsYandexMarkers;

ymaps.ready(init);
function init() {
	yandexMap = new ymaps.Map("Map", {
    center: [55.653465, 37.823784],
    zoom: 13,
  });

  let markerCollection, multiRoute;

  function drawMarkers (markers) {
		// Создание макета балуна
	  let MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
	    '<div class="Balloon Balloon-top">' +
	    '<a class="Balloon_close" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/><path d="M0 0h24v24H0z" fill="none"/></svg></a>' +
	    '<div class="Balloon_arrow"></div>' +
	    '<div class="Balloon_inner">' +
	    '$[[options.contentLayout observeSize maxHeight=350]]' +
	    '</div>' +
	    '</div>', {
	    build: function () {
	      this.constructor.superclass.build.call(this);
	      this._$element = $('.Balloon', this.getParentElement());
	      this.applyElementOffset();
	      this._$element.find('.Balloon_close').on('click', $.proxy(this.onCloseClick, this));
	    },

	    clear: function () {
	      this._$element.find('.Balloon_close').off('click');
	      this.constructor.superclass.clear.call(this);
	    },

	    onSublayoutSizeChange: function () {
	      MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);
	      if(!this._isElement(this._$element)) {
	      	return;
	      }
	      this.applyElementOffset();
	      this.events.fire('shapechange');
	    },

	    applyElementOffset: function () {
	      this._$element.css({
	        left: -(this._$element[0].offsetWidth / 2),
	        top: -(this._$element[0].offsetHeight + this._$element.find('.Balloon_arrow')[0].offsetHeight)
	      });
	    },

	    onCloseClick: function (e) {
	      e.preventDefault();
	      this.events.fire('userclose');
	    },

	    getShape: function () {
	      if(!this._isElement(this._$element)) {
	        return MyBalloonLayout.superclass.getShape.call(this);
	      }

	      var position = this._$element.position();

	      return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
	        [position.left, position.top], [
	          position.left + this._$element[0].offsetWidth,
	          position.top + this._$element[0].offsetHeight + this._$element.find('.Balloon_arrow')[0].offsetHeight
	        ]
	      ]));
	    },

	    _isElement: function (element) {
	      return element && element[0] && element.find('.Balloon_arrow')[0];
	    }
	  });

		// Создание вложенного макета содержимого балуна.
	  let MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
	    '<h3 class="Balloon_title">$[properties.balloonHeader]</h3>' +
	    '<div class="Balloon_content">$[properties.balloonContent]</div>'
	  );

		markerCollection = new ymaps.GeoObjectCollection({}, {
			iconLayout: 'default#image',
			hideIconOnBalloonOpen: false,
		});

		yandexMarkers.forEach(item => {
			if (item.hasOwnProperty('flag')) {
				markerCollection.add(new ymaps.Placemark([item.coords.lat, item.coords.lng], {
		    	hintContent: item.title,
		    	markerType: item.type,
		    	flag: item.flag,
		    	ico2: item.ico2
		    }, {
		    	iconImageHref: item.ico,
		    	iconImageSize: [44, 46]
		    }));
			} else {
				markerCollection.add(new ymaps.Placemark([item.coords.lat, item.coords.lng], {
		    	hintContent: item.title,
		    	balloonContent: item.title,
		    	markerType: item.type,
		    	flag: item.flag,
		    	ico2: item.ico2
		    }, {
		    	iconImageHref: item.ico,
		    	iconImageSize: [44, 46],
		    	balloonOffset: [8, -24],
		    	balloonLayout: MyBalloonLayout,
		    	balloonContentLayout: MyBalloonContentLayout,
		    }));
			}
		});

		markerCollection.add(new ymaps.Placemark([55.638661, 37.845971], {
	  	hintContent: "ЖК «Новые Котельники»",
	  	balloonContent: "ЖК «Новые Котельники»",
	  }, {
	  	iconImageHref: "/img/mappins/logo_mark.png",
	  	iconImageSize: [74, 75],
	  	balloonOffset: [23, -24],
	  	balloonLayout: MyBalloonLayout,
	  	balloonContentLayout: MyBalloonContentLayout,
	  }));

		yandexMap.geoObjects.add(markerCollection);
	}

  function hideYandexMarkers (type) {
  	markerCollection.each(item => {
	  	console.log(item.properties.getAll());
	  	if (type === item.properties.get('markerType')) {
	  		item.options.set('visible', false);
	  	}
	  });
  }

  function showYandexMarkers (type) {
  	markerCollection.each(item => {
	  	console.log(item.properties.getAll());
	  	if (type === item.properties.get('markerType')) {
	  		item.options.set('visible', true);
	  	}
	  });
  }

  function setDefaultMarkerImage () {
  	markerCollection.each(item => {
  		if (item.properties.get('flag')) {
  			if (item.properties.get('flag') == 2) {
  				let bufferImg = item.properties.get('ico2');
					item.properties.set('ico2', item.options.get('iconImageHref'));
					item.options.set('iconImageHref', bufferImg);
					item.options.set('iconImageSize', [44, 46]);
					item.options.set('iconImageOffset', [-12, -40]);
					item.properties.set('flag', 1);
  			}
  		}
  	});
  }

	drawMarkers(yandexMarkers);

	// При клике на карту все метки будут удалены.
	markerCollection.getMap().events.add('click', function() {
    console.log('Click on map!');
	});

	markerCollection.each(item => {
  	item.events.add('click', function (e) {
  		console.log(item.options.get('iconImageHref'));
  		if (item.properties.get('flag')) {
  			if (item.properties.get('flag') == 1) {
  				setDefaultMarkerImage();
  				let bufferImg = item.options.get('iconImageHref');
  				item.options.set('iconImageHref', item.properties.get('ico2'));
  				item.properties.set('ico2', bufferImg);
					item.options.set('iconImageSize', [230, 240]);
					item.options.set('iconImageOffset', [-110, -240]);
					item.properties.set('flag', 2);
  			} else if (item.properties.get('flag') == 2) {
  				let bufferImg = item.properties.get('ico2');
  				item.properties.set('ico2', item.options.get('iconImageHref'));
  				item.options.set('iconImageHref', bufferImg);
					item.options.set('iconImageSize', [44, 46]);
					item.options.set('iconImageOffset', [-12, -40]);
					item.properties.set('flag', 1);
  			}
	  	}
  	});
  });

	$("[data-marker-type]").click(function() {
		let markerType = $(this).data("marker-type");

		$(this).toggleClass("isOff");

		if ($(this).hasClass("isOff")) {
			hideYandexMarkers(+markerType);
		} else {
			showYandexMarkers(+markerType);
		}
	});

	yandexMap.controls.remove('geolocationControl');
  yandexMap.controls.remove('searchControl');
  yandexMap.controls.remove('trafficControl');
  yandexMap.controls.remove('typeSelector');
  yandexMap.controls.remove('fullscreenControl');
  yandexMap.controls.remove('rulerControl');
  yandexMap.behaviors.disable(['scrollZoom']);
}

// END: infra map

let mapConfig = {
	logo: {
		title: "ЖК «Новые Котельники»",
		icon: {
			url: "/img/mappins/logo_mark.png",
			scaledSize: '',
		},
		details: {
			type: 999
		},
		lat: APP_CONFIGS.buildingLatLng.lat,
		lng: APP_CONFIGS.buildingLatLng.lng
	},
	to: [
		{
			title: "Куда",
			icon: {
				url: "/img/mappins/dot-purple.svg",
				scaledSize: '',
			},
			lat: 55.638022,
			lng: 37.846356
		},
		{
			title: "Куда",
			icon: {
				url: "/img/mappins/dot-green.svg",
				scaledSize: '',
			},
			lat: 55.638022,
			lng: 37.846356
		},
		{
			title: "Куда",
			icon: {
				url: "/img/mappins/dot-blue.svg",
				scaledSize: '',
			},
			lat: 55.638022,
			lng: 37.846356
		}
	],
	from: [
		[
			{
				title: "Котельники",
				icon: {
					url: "/img/mappins/metro-kot.png",
					scaledSize: '',
				},
				lat: 55.675069,
				lng: 37.858625
			},
			{
				title: "Люблино",
				icon: {
					url: "/img/mappins/metro-lub.png",
					scaledSize: '',
				},
				lat: 55.675737,
				lng: 37.761881
			},
			{
				title: "Алма-Атинская",
				icon: {
					url: "/img/mappins/metro-alm.png",
					scaledSize: '',
				},
				lat: 55.632815,
				lng: 37.766049
			}
		],
		{
			title: "МКАД",
			icon: {
				url: "/img/mappins/dot-purple.svg",
				scaledSize: '',
			},
			lat: 55.639382,
			lng: 37.823559
		},
		{
			title: "Новорязанское шоссе",
			icon: {
				url: "/img/mappins/dot-purple.svg",
				scaledSize: '',
			},
			lat: 55.662944,
			lng: 37.877704
		},
		{
			title: "Домодедово",
			icon: {
				url: "/img/mappins/dot-purple.svg",
				scaledSize: '',
			},
			lat: 55.418625,
			lng: 37.89514
		}
	]
};


{
	for (let i = 0, length = $(`[data-src^="#MapPopup"]`).length; i < length; i++) {
		let map;
		let wrapper = document.querySelector(`#MapPopup${i}`);
		let mapEl = wrapper.querySelector(`.MapPopup_map`);

		$(`[data-fancybox='MapPopup${i}']`).fancybox({
			idleTime: 0,
			btnTpl: {
				smallBtn:
					'<button data-fancybox-close class="Btn Btn-modalClose Btn-features" title="{{CLOSE}}"><svg class="SvgIco" height="13" viewBox="0 0 13 13" width="13" xmlns="http://www.w3.org/2000/svg"><path class="SvgIco_path" d="m0 10.21h2.79v2.79h1.85v-4.64h-4.64zm2.79-7.42h-2.79v1.85h4.64v-4.64h-1.85zm5.57 10.21h1.85v-2.79h2.79v-1.85h-4.64zm1.85-10.21v-2.79h-1.85v4.64h4.64v-1.85z"/></svg></button>'
			},
			afterLoad: function() {
				// ...
				map = new ymaps.Map(mapEl, {
					center: [55.639457, 37.855543],
					zoom: 13,
				});

				map.geoObjects
					.add(new ymaps.Placemark([mapConfig.to[0].lat, mapConfig.to[0].lng], {
							balloonContent: mapConfig.to[0].title,
					}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: mapConfig.to[0].icon.url,
							// Размеры метки.
							iconImageSize: [18, 18],
							// Смещение левого верхнего угла иконки относительно
							// её "ножки" (точки привязки).
							iconImageOffset: [-9, -9]
					}))

					.add(new ymaps.Placemark([mapConfig.logo.lat, mapConfig.logo.lng], {
							balloonContent: mapConfig.logo.title,
					}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: mapConfig.logo.icon.url,
							// Размеры метки.
							iconImageSize: [74, 75],
							// Смещение левого верхнего угла иконки относительно
							// её "ножки" (точки привязки).
							iconImageOffset: [-36, -36]
					}));

					if (i === 0) {
						console.log(mapConfig.from[0]);
						map.geoObjects.add(new ymaps.Placemark([mapConfig.from[0][i].lat, mapConfig.from[0][i].lng], {
							balloonContent: mapConfig.from[0][i].title,
					}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: mapConfig.from[0][i].icon.url,
							// Размеры метки.
							iconImageSize: [128, 38],
					}))

					// Построение маршрута.
						let multiRoute = new ymaps.multiRouter.MultiRoute({   
							referencePoints: [
								[mapConfig.from[0][i].lat, mapConfig.from[0][i].lng],
								[mapConfig.to[0].lat, mapConfig.to[0].lng],
							]
						}, {
								routeActiveStrokeWidth: 4,
								routeActiveStrokeColor: "#B2278C"
						});

						// Добавление маршрута на карту.
						map.geoObjects.add(multiRoute);

					} else if (i > 0) {
						
						map.geoObjects.add(new ymaps.Placemark([mapConfig.from[i].lat, mapConfig.from[i].lng], {
							balloonContent: mapConfig.from[i].title,
					}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: mapConfig.from[i].icon.url,
							// Размеры метки.
							iconImageSize: [18, 18],
					}))

					// Построение маршрута.
						let multiRoute = new ymaps.multiRouter.MultiRoute({   
							referencePoints: [
								[mapConfig.from[i].lat, mapConfig.from[i].lng],
								[mapConfig.to[0].lat, mapConfig.to[0].lng],
							]
						}, {
								routeActiveStrokeWidth: 4,
								routeActiveStrokeColor: "#B2278C"
						});

						// Добавление маршрута на карту.
						map.geoObjects.add(multiRoute);
					}

				$(wrapper).tabs({
					active: 0,
					classes: {
						"ui-tabs-active": "MapPopup_tab-active"
					},
					activate: (event, ui) => {
						
						map.geoObjects.removeAll();
						if (
							$(ui.newTab)
								.find(".MapPopup_tabLink")
								.prop("href")
								.match(/tabSection-1/g)
						) {
							
							map.geoObjects
								.add(new ymaps.Placemark([mapConfig.from[0][0].lat, mapConfig.from[0][0].lng], {
									balloonContent: mapConfig.from[0][0].title,
									}, {
											// Опции.
											// Необходимо указать данный тип макета.
											iconLayout: 'default#image',
											// Своё изображение иконки метки.
											iconImageHref: mapConfig.from[0][0].icon.url,
											// Размеры метки.
											iconImageSize: [128, 38],
											// Смещение левого верхнего угла иконки относительно
											// её "ножки" (точки привязки).
											// iconImageOffset: [-36, -36]
									}))
								.add(new ymaps.Placemark([mapConfig.to[0].lat, mapConfig.to[0].lng], {
									balloonContent: mapConfig.to[0].title,
									}, {
											// Опции.
											// Необходимо указать данный тип макета.
											iconLayout: 'default#image',
											// Своё изображение иконки метки.
											iconImageHref: mapConfig.to[0].icon.url,
											// Размеры метки.
											iconImageSize: [18, 18],
											// Смещение левого верхнего угла иконки относительно
											// её "ножки" (точки привязки).
											iconImageOffset: [-9, -9]
									}))
								
									let multiRoute = new ymaps.multiRouter.MultiRoute({   
										referencePoints: [
											[mapConfig.from[0][0].lat, mapConfig.from[0][0].lng],
											[mapConfig.to[0].lat, mapConfig.to[0].lng],
										]
									}, {
											routeActiveStrokeWidth: 4,
											routeActiveStrokeColor: "#B2278C"
									});
			
									// Добавление маршрута на карту.
									map.geoObjects.add(multiRoute);
									
						} else if (
							$(ui.newTab)
								.find(".MapPopup_tabLink")
								.prop("href")
								.match(/tabSection-2/g)
						) {
							map.geoObjects
								.add(new ymaps.Placemark([mapConfig.from[0][1].lat, mapConfig.from[0][1].lng], {
									balloonContent: mapConfig.from[0][1].title,
									}, {
											// Опции.
											// Необходимо указать данный тип макета.
											iconLayout: 'default#image',
											// Своё изображение иконки метки.
											iconImageHref: mapConfig.from[0][1].icon.url,
											// Размеры метки.
											iconImageSize: [128, 38],
											// Смещение левого верхнего угла иконки относительно
											// её "ножки" (точки привязки).
											// iconImageOffset: [-36, -36]
									}))
								.add(new ymaps.Placemark([mapConfig.to[0].lat, mapConfig.to[0].lng], {
									balloonContent: mapConfig.to[0].title,
									}, {
											// Опции.
											// Необходимо указать данный тип макета.
											iconLayout: 'default#image',
											// Своё изображение иконки метки.
											iconImageHref: mapConfig.to[0].icon.url,
											// Размеры метки.
											iconImageSize: [18, 18],
											// Смещение левого верхнего угла иконки относительно
											// её "ножки" (точки привязки).
											iconImageOffset: [-9, -9]
									}))
								
									let multiRoute = new ymaps.multiRouter.MultiRoute({   
										referencePoints: [
											[mapConfig.from[0][1].lat, mapConfig.from[0][1].lng],
											[mapConfig.to[0].lat, mapConfig.to[0].lng],
										]
									}, {
											routeActiveStrokeWidth: 4,
											routeActiveStrokeColor: "#B2278C"
									});
			
									// Добавление маршрута на карту.
									map.geoObjects.add(multiRoute);
						} else if (
							$(ui.newTab)
								.find(".MapPopup_tabLink")
								.prop("href")
								.match(/tabSection-3/g)
						) {
							map.geoObjects
								.add(new ymaps.Placemark([mapConfig.from[0][2].lat, mapConfig.from[0][2].lng], {
									balloonContent: mapConfig.from[0][1].title,
									}, {
											// Опции.
											// Необходимо указать данный тип макета.
											iconLayout: 'default#image',
											// Своё изображение иконки метки.
											iconImageHref: mapConfig.from[0][2].icon.url,
											// Размеры метки.
											iconImageSize: [128, 38],
											// Смещение левого верхнего угла иконки относительно
											// её "ножки" (точки привязки).
											// iconImageOffset: [-36, -36]
									}))
								.add(new ymaps.Placemark([mapConfig.to[0].lat, mapConfig.to[0].lng], {
									balloonContent: mapConfig.to[0].title,
									}, {
											// Опции.
											// Необходимо указать данный тип макета.
											iconLayout: 'default#image',
											// Своё изображение иконки метки.
											iconImageHref: mapConfig.to[0].icon.url,
											// Размеры метки.
											iconImageSize: [18, 18],
											// Смещение левого верхнего угла иконки относительно
											// её "ножки" (точки привязки).
											iconImageOffset: [-9, -9]
									}))
								
									let multiRoute = new ymaps.multiRouter.MultiRoute({   
										referencePoints: [
											[mapConfig.from[0][2].lat, mapConfig.from[0][2].lng],
											[mapConfig.to[0].lat, mapConfig.to[0].lng],
										]
									}, {
											routeActiveStrokeWidth: 4,
											routeActiveStrokeColor: "#B2278C"
									});
			
									// Добавление маршрута на карту.
									map.geoObjects.add(multiRoute);
						}
					}
				});
			},

			afterClose: function() {
				$(wrapper).tabs("destroy");
				map.destroy();
			}
		});
	}
}
