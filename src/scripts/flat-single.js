new FilterForm("#filterForm", {
	submitHandler: $.noop
});

$("[data-before-after]").beforeAfter();

$("#makeOrderBtn").on("click", function(event) {
	event.preventDefault();
	$.fancybox.open($("#orderModal"), {
		autoCenter: true
	});
});

{
	let activeClassName = "_active";
	let $tabs = $("[data-slider-tabs-link]");

	$tabs.on("click", function(event) {
		event.preventDefault();

		let $link = $(this);
		let $blocks = $("[data-slider-tabs-block]");
		let href = $link.attr("href");

		if ($link.hasClass(activeClassName)) return;

		$tabs.removeClass(activeClassName);
		$link.addClass(activeClassName);

		$blocks.filter(":visible").animate(
			{
				opacity: 0
			},
			100,
			function() {
				$(this).hide();

				$tabs.removeClass(activeClassName);
				$link.addClass(activeClassName);

				$blocks
					.filter(href)
					.show()
					.animate({
						opacity: 1
					});
			}
		);
	});
}

$("[data-toggle-filter-panel]").on("click", event => {
	event.preventDefault();
	$(".FiltersSection_rightCol").toggleClass("FiltersSection_rightCol-active");
});

new InteractiveForm(document.querySelector(".FormModal_form"));