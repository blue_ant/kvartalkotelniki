/*=require ./includes/chunks/APP_CONFIGS.js */
/*=require ./includes/chunks/fancybox_defaults.js */
/*=require ./includes/chunks/gallery_show_social.js */
/*=require ./includes/chunks/gallery_adjust_height.js */
/*=require ./includes/chunks/gallery_controls_slides_bind.js */
/*=require ./includes/blocks/*.js */

// TODO: aggregate these next three functions to FilterForm class

function prepareFilterFormValues(valsObj) {
	return _.mapValues(valsObj, param => {
		// convert all strings and arrays from inputs to 'INT' type
		let result;
		if (_.isString(param)) {
			result = parseInt(param);
		} else if (_.isArray(param)) {
			result = _.map(param, p => {
				return parseInt(p);
			});
		}
		return result;
	});
}

function saveFilterFormInputs() {
	let savedJSON = localStorage.getItem("filterFormState");
	let valsToStore;
	if (savedJSON) {
		valsToStore = JSON.parse(savedJSON);
	} else {
		valsToStore = {};
	}

	let $inps = $("[data-persist-id]");

	$inps.each(function(index, el) {
		let inpID = el.dataset.persistId;
		if (el.type && el.type === "checkbox") {
			valsToStore[inpID] = !!el.checked;
		} else if (el.tagName === "SELECT") {
			let tmpSelectedOptions = [];

			for (let i = el.options.length - 1; i >= 0; i--) {
				let opt = el.options[i];
				if (opt.value && opt.selected) {
					tmpSelectedOptions.push(opt.value);
				}
			}

			valsToStore[inpID] = tmpSelectedOptions;
		} else {
			valsToStore[inpID] = el.value;
		}
	});

	localStorage.setItem("filterFormState", JSON.stringify(valsToStore));
	console.log("saved form states:", valsToStore);
}

function restoreFilterFormInputs() {
	let storedValues = JSON.parse(localStorage.getItem("filterFormState"));
	if (!storedValues) return false;
	console.log("restored form states:", storedValues);
	let $inps = $("[data-persist-id]");

	$inps.each(function(index, el) {
		let inpID = el.dataset.persistId;
		if (el.type && el.type === "checkbox") {
			let isChecked = !!storedValues[el.dataset.persistId];
			el.checked = isChecked;
		} else if (el.tagName === "SELECT") {
			if (storedValues[inpID]) {
				let selectedOptsValues = storedValues[inpID];
				for (var i = 0; i < el.options.length; i++) {
					let opt = el.options[i];
					if (selectedOptsValues.includes(opt.value)) {
						opt.selected = "selected";
					}
				}
			}
		} else {
			el.value = storedValues[inpID];
		}
	});
}

/*function resetFilterFormInputs(){
	localStorage.removeItem("filterFormState");
}*/

const currentDevice = device.default;

let dimensionValue = {
	mobile: 767,
	tablet: 1365
};

// START: main menu behaviour
$(".Burger,.MobMenu_overlay").on("click", event => {
	event.preventDefault();
	$(".MobMenu,.BigMenu").toggle();
	$(".Burger_pic").toggleClass("Burger_pic-menuOpen");
	if ($(".MobMenu").is(":visible")) {
		$(document.body).css("overflow", "hidden");
	} else {
		$(document.body).removeAttr("style");
	}
});

$(".MobMenu_link-withSubmenu").on("click", event => {
	event.preventDefault();
	let $targetSubmenu = $(event.currentTarget.hash);
	if ($targetSubmenu.length) {
		$("#topLevelMobMenu").removeClass("MobMenu_list-active");
		$targetSubmenu.addClass("MobMenu_list-active");
	} else {
		console.error(`Error: target submenu element not found (${event.currentTarget.hash})`);
	}
});

$(".MobMenu_link-toBack").on("click", event => {
	event.preventDefault();
	$(".MobMenu_list-active").removeClass("MobMenu_list-active");
	$("#topLevelMobMenu").addClass("MobMenu_list-active");
});

$(".BigMenu_textCutter")
	.on("mouseover", function() {
		$(this).addClass("BigMenu_textCutter-active");
		$(".BigMenu_unitsContainer").addClass("BigMenu_unitsContainer-active");
	})
	.on("mouseout", function() {
		$(this).removeClass("BigMenu_textCutter-active");
		$(".BigMenu_unitsContainer").removeClass("BigMenu_unitsContainer-active");
	});
// END: main menu behaviour

$(document).ready(function() {
	window.pagePreloader = new Preloader("#pagePreloader");
	window.pagePreloader.hide();

	$("[data-counter]").each(function() {
		let $counter = $(this);
		let curPos = $counter.data("counter");
		let $inner = $counter.find(".header-counter__inner");
		let $info = $counter.find(".header-counter__info");
		let $percent = $info.find("div:last");

		$inner.css({
			width: curPos + "%"
		});

		$percent.html(curPos + "%");
		$counter.show();

		if ($inner.width() + $info.outerWidth() > $counter.width()) {
			$info.addClass("_right");
		} else if ($inner.width() < $info.outerWidth() / 2) {
			$info
				.css({
					marginRight: $inner.width() - $info.outerWidth() / 2 - $inner.width()
				})
				.addClass("_left");
		}
	});
});

$(window).on('load', function () {
	new InteractiveForm(document.querySelector(".FeedbackForm"));
})

// new InteractiveForm(document.querySelector(".FeedbackForm"), {
// 	// submitHandler: form => {
	// 	let $form = $(form);
	// 	console.log('form')
	// 	console.log($form, $form.eq(0).serializeObject())
	// 	window.pagePreloader.show();

	// 	let dataToSend = $.extend(true, $form.eq(0).serializeObject(), {
	// 		Submit: 1,
	// 		url: window.location.href
	// 	});

	// 	$.ajax({
	// 		url: form.action,
	// 		type: form.method,
	// 		data: dataToSend,
	// 		success: function(response) {
	// 			let errorCode = parseInt(response.code);

	// 			if (errorCode === 0) {
	// 				let successText = `<div class="FormSuccess"><div class="FormSuccess__text ">${
	// 					response.success
	// 				}</div></div>`;

	// 				window.requestAnimationFrame(() => {
	// 					$form.hide().after(successText);
	// 				});

	// 				if (typeof window["ctCheckCallbackShouldBeProcessed"] == "function") {
	// 					if (ctCheckCallbackShouldBeProcessed()) {
	// 						let phone = $form
	// 							.find("input[type=tel]")
	// 							.val()
	// 							.replace(/[^0-9\+]/gim, "");

	// 						console.log(phone);

	// 						ctSendCallbackRequest(phone);

	// 						var timer = setInterval(function() {
	// 							console.log(ctGetCallbackRequestStatus());

	// 							if (ctGetCallbackRequestStatus() != "Попытка отправки заявки на обратный звонок.") {
	// 								clearInterval(timer);

	// 								console.log(ctGetCallbackRequestStatus());
	// 							}
	// 						}, 500);
	// 					}
	// 				}
	// 			} else {
	// 				alert("Не удалось отправить форму! Попробуйте позже или обратитесть по телефону...");
	// 			}

	// 			window.pagePreloader.hide();
	// 		}
	// 	});
	// }
// });

$(".Subscribe_form").each(function(index, el) {
	new InteractiveForm(el);
});

(function() {
	$("[data-fancy]").each(function() {
		let $this = $(this);
		let url = $this.attr("href");

		$this.on("click", function(e) {
			e.preventDefault();

			$.fancybox.open($(url), {
				autoCenter: true,
				baseClass: "_custom"
			});

			new InteractiveForm($(url).find("form"));
		});
	});
})();

// Privacy Policy Scroll
const ps = new PerfectScrollbar(".PrivacyPolicy");

$('[data-src="#PrivacyPolicyPopup"]').fancybox({
	baseTpl:
		'<div class="fancybox-container FancyboxContainer FancyboxContainer-privacyPolicy" role="dialog" tabindex="-1">' +
		'<div class="fancybox-bg"></div>' +
		'<div class="fancybox-inner">' +
		'<div class="fancybox-infobar"><span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span></div>' +
		'<div class="fancybox-toolbar FancyboxToolbar">{{buttons}}</div>' +
		'<div class="fancybox-navigation">{{arrows}}</div>' +
		'<div class="fancybox-stage"></div>' +
		'<div class="fancybox-caption"></div>' +
		"</div>" +
		"</div>",

	btnTpl: {
		smallBtn:
			'<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small FancyboxCloseBtn" title="{{CLOSE}}">' +
			'<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17"><g fill="#5B6573"><path d="M1.485.071l15.557 15.556-1.415 1.415L.071 1.485z"/><path d="M15.627.071l1.415 1.414L1.485 17.042.071 15.627z"/></g></svg>' +
			"</button>"
	},

	slideClass: "FancyboxSlide",

	afterLoad: function() {
		console.log("After Load!");
		ps.update();
	}
});

$('.CmsEditorOutput_callback').click(function(){
	window.Calltouch.Callback.onClickCallButton();
	return false;
});