$(document).ready(function() {
	let slider = new Swiper(".GalleryShow", {
		navigation: {
			nextEl: ".swiper-button-next",
			prevEl: ".swiper-button-prev"
		},
		on: {
			slideChangeTransitionStart: function() {
				if (this.activeIndex + 1 < 10) {
					$(".GalleryShow_counter").html("0" + (this.activeIndex + 1));
				} else {
					$(".GalleryShow_counter").html(this.activeIndex + 1);
				}
			},
			init: function() {
				$(".GalleryShow_counter").html("01");
			}
		}
	});

	let $slideWithImg = $(
		".GalleryShow_slide:not(.GalleryShow_slide-webcam):not(.GalleryShow_slide-video):not(.GalleryShow_slide-panorama)"
	);

	$slideWithImg.click(() => {
		slider.slideNext();
	});

	// Changing pack of slides
	{
		$("[data-trigger-pack]").click(function() {
			$("[data-slides-pack]").attr("hidden", "true");
			$(`[data-slides-pack="${$(this).data("trigger-pack")}"]`).removeAttr("hidden");
			slider.update();
		});
	}

	// Change active state of mobile tab
	GalleryMobileTabs(slider);

	// Sidebar scroll
	{
		let $sidebar = $(".GallerySidebar"),
			$window = $(window),
			isInited = false;

		function initNiceScrollVert(el) {
			el.niceScroll({
				cursorcolor: "linear-gradient(178deg, #86c98b, #7dcac5)",
				cursorfixedheight: 114,
				cursoropacitymin: 1,
				cursorwidth: 4,
				preventmultitouchscrolling: false,
				horizrailenabled: true,
				cursorborder: "0px solid transparent",
				scrollspeed: 200,
				cursorborderradius: "0"
			});
		}

		function initNiceScrollHor(el) {
			el.niceScroll({
				cursorcolor: "linear-gradient(178deg, #86c98b, #7dcac5)",
				cursorfixedheight: 114,
				cursoropacitymin: 1,
				cursorwidth: 5,
				preventmultitouchscrolling: true,
				horizrailenabled: true,
				cursorborder: "0px solid transparent",
				scrollspeed: 200,
				cursorborderradius: "0",
				railvalign: "top",
				railpadding: { top: 5, right: 0, left: 0, bottom: 0 }
			});
		}

		if ($window.width() > dimensionValue.tablet) {
			initNiceScrollVert($sidebar);
			isInited = true;
		} else if ($window.width() < dimensionValue.tablet && $window.width() > dimensionValue.mobile) {
			initNiceScrollHor($sidebar);
			isInited = true;
		}

		$(".nicescroll-cursors").addClass("GallerySidebarScroll");

		$(window).resize(function() {
			if ($window.width() < dimensionValue.tablet && $window.width() > dimensionValue.mobile) {
				$sidebar.getNiceScroll().hide();
				isInited = false;
			} else if ($window.width() > dimensionValue.tablet) {
				if (!isInited) {
					initNiceScrollVert($sidebar);
					isInited = true;
				}
			}
		});
	}

	// Block's text in sidebar clip with ellipsis
	{
		let $dotsText = $(".GallerySidebar_text"),
			dotsOptions = {
				fallbackToLetter: true,
				watch: true,
				height: 60
			};

		let dots;

		if ($(window).width() > dimensionValue.tablet) {
			dots = new Dots($dotsText, dotsOptions);
		}

		(function() {
			var $link = $(".GallerySidebar_more");

			$link.click(function(e) {
				e.preventDefault();

				setTimeout(function() {
					$(".GallerySidebar")
						.getNiceScroll()
						.resize();
				}, 300);

				if (
					$(this)
						.closest(".GallerySidebar_info")
						.hasClass("GallerySidebar_info-expanded")
				) {
					$(this).html("Подробнее");
					$(this)
						.closest(".GallerySidebar_info")
						.removeClass("GallerySidebar_info-expanded");

					dots.truncate();

					return false;
				}

				$(this)
					.html("Скрыть")
					.addClass("GallerySidebar_more-shrink");
				$(this)
					.closest(".GallerySidebar_info")
					.addClass("GallerySidebar_info-expanded");

				dots.restore();
			});
			$(window).resize(function() {
				$(".GallerySidebar")
					.getNiceScroll()
					.resize();
				$(".GallerySidebar_item .GallerySidebar_info").removeClass("GallerySidebar_info-expanded");

				if ($(window).width() > dimensionValue.tablet) {
					$link.html("Подробнее");
					$link.removeClass("GallerySidebar_more-shrink");

					if (!dots) {
						dots = new Dots($dotsText, dotsOptions);
					} else {
						dots.truncate();
					}
				} else {
					if (dots) {
						dots.restore();
					}
				}
			});

			$(".GallerySidebar_linkArea").click(function() {
				if ($(".GallerySidebar_info").hasClass("GallerySidebar_info-expanded")) {
					dots.truncate();

					$link.html("Подробнее");

					$(".GallerySidebar_info").removeClass("GallerySidebar_info-expanded");
				}
			});
		})();
	}

	// Gallery fullscreen toggle
	{
		let $trigger = $(".GalleryShow_resize"),
			$sidebar = $(".GallerySidebar"),
			counter = 0;

		$trigger.click(function() {
			$(".Content").toggleClass("Content-galleryFull");

			$("body, html").animate(
				{
					scrollTop: 0
				},
				300
			);
			let intervalCounter = setInterval(function() {
				if (counter !== 3) {
					counter++;
				} else {
					counter = 0;
					clearInterval(intervalCounter);
				}
			}, 100);
			let intervalUpdatingSlider = setInterval(function() {
				if (counter === 3) {
					clearInterval(intervalUpdatingSlider);
				}
				slider.update();
				$sidebar.getNiceScroll().resize();
			}, 0);
		});
	}

	// Sidebar blocks toggle
	{
		let $el = $(".GallerySidebar_linkArea");

		$el.click(function() {
			$el.closest(".GallerySidebar_item").removeClass("GallerySidebar_item-expanded");
			$(this)
				.closest(".GallerySidebar_item")
				.toggleClass("GallerySidebar_item-expanded");

			slider.slideTo(0);

			setTimeout(function() {
				$(".GallerySidebar")
					.getNiceScroll()
					.resize();
			}, 300);
		});
	}

	//Mobile social expansion
	{
		let $trigger = $(".GalleryShow_socialExtend"),
			$social = $(".GalleryShow_socialLinks");

		showSocial($trigger, $social, "GalleryShow_socialLinks-active");
	}
});
