let sectionsSlider = new SectionsSlider("#chessSlider");
let chessBuilder = new Chess();

let houseID = parseInt(window.location.pathname.split("house-")[1]);

function idxOfGroup(prefix, array) {
	const regex = new RegExp(`${prefix}-.+$`);
	return array.findIndex(i => regex.test(i));
}

$.ajax({
	url: `https://kvartalkotelniki.ru/choose/genplan/house-${houseID}/`,
	//url: '/ajax/chess_scheme_example.json',
	dataType: "json",
	method: "GET",
	data: {
		action: "get_flats"
	}
})
    .done(jsonResponse => {
        let slides = [];
        jsonResponse.sections.forEach(sect => {
            slides.push(chessBuilder.renderSection(sect));
        });

        const initialSlide = Math.floor(slides.length / 2);

		sectionsSlider.setSlides(slides);
		sectionsSlider.setInitialSlide(initialSlide);

        if (currentDevice.type === "desktop") {
            sectionsSlider.$el.find(".SectionsSlider_track").tooltip({
                items: ".Chess_flatlink-active",
                hide: 200,
                show: 200,
                content: function () {
                    return this.getElementsByTagName("template")[0].innerHTML;
                }
            });
        }
        // START: filters behaviour
        {
            //TODO: create separate class for chess section filter
            let $flatsLinks = sectionsSlider.$el.find(".Chess_flatlink");
            let $form = $("#BtnsLegend").find("form");

            $("#BtnsLegend").find(".BtnsLegend_inner").tooltip({
                classes: {
                    "ui-tooltip": 'BtnsLegend_tooltip',
                },
                items: ".BtnsLegend_btn",
                hide: 200,
                show: 200,
                position: {
                    my: 'center top',
                    at: 'center bottom+5',
                    collision: 'fit',
                },
                content: function () {
                    return $(this).attr('title');
                }
            });

			$("#BtnsLegend")
				.find(".BtnsLegend_inp")
				.checkboxradio({
					icon: false,
					classes: {
						"ui-checkboxradio-label": "",
						"ui-checkboxradio-checked": "BtnsLegend_btn-active"
					}
				})
				.on("change", event => {
					event.preventDefault();
					if (event.target.name.includes('finish')) {
						$('input[name^=finish]').each(function() {
							if ($(this).parent().hasClass('BtnsLegend_btn-active') && this !== event.target) {
								$(this).prop('checked', false).checkboxradio('refresh');
							}
						});
					}
					window.requestAnimationFrame(() => {
						let activeFlatSelectors = [];
                        let finishFilter = '';
						$form.serializeArray().forEach(nameValueObj => {
                            if (!/full|whitebox$/.test(nameValueObj.name)) {
                                activeFlatSelectors.push(`.Chess_flatlink-${nameValueObj.name}`);
                            } else {
                                finishFilter = `.Chess_flatlink-${nameValueObj.name}`;
                            }
						});
						if (activeFlatSelectors.length && finishFilter) {
							$flatsLinks
								.removeClass("Chess_flatlink-active")
								.filter(activeFlatSelectors.join(","))
                                .filter(finishFilter)
								.addClass("Chess_flatlink-active");
						} else if (activeFlatSelectors.length || finishFilter) {
							$flatsLinks
								.removeClass("Chess_flatlink-active")
								.filter(activeFlatSelectors.join(",") || finishFilter)
								.addClass("Chess_flatlink-active");
						} else {
							$flatsLinks.addClass("Chess_flatlink-active");
						}
					});
				});
		}
		// END: filters behaviour
	})
	.fail(() => {
		alert("Не удалось получить данные!\nПопробуйте позже или обратитесь к администрации сайта.");
	});
