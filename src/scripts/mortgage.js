let banksListRenderer = new BankList();
let $containerForBanksList = $("#banksListContainer");

function checkBank(singleBankParams, opts) {
	let price = opts.price;
	let firstPay = opts.firstPay;
	let duration = opts.duration;

	// check by first pay party
	let firsPayPartyPercent = Math.round(firstPay / (price / 100));

	if (!singleBankParams.firstTo) singleBankParams.firstTo = 100;

	if (firsPayPartyPercent < singleBankParams.firstFrom || firsPayPartyPercent > singleBankParams.firstTo) return false;

	// check by pay duration range
	if (!_.inRange(duration, singleBankParams.yearsFrom, singleBankParams.yearsTo + 1)) return false;

	return true;
}

var pickupBanks = _.debounce(() => {
	let allBanksParams = window.banksMortgageConditions;
	let selectedOpts = $calculatorForm.serializeObject();

	// convert all selected values to 'INT' type
	selectedOpts = _.mapValues(selectedOpts, param => {
		return parseInt(param);
	});

	let acceptableBanks = [];

	// filter banks
	allBanksParams.forEach(bankParams => {
		if (checkBank(bankParams, selectedOpts)) {
			acceptableBanks.push(bankParams);
		}
	});

	// render banks
	let listMarkup = banksListRenderer.renderList({
		banks: acceptableBanks,
		opts: selectedOpts
	});

	$containerForBanksList.html(listMarkup);
}, 100);

var $calculatorForm = $(".MortgageSliders");
$calculatorForm.on("submit", function(event) {
	event.preventDefault();
	return false;
});
// SLIDERS START ----------

{
	let $formSliderBlock = $calculatorForm.find(".MortgageSliders_option-firstPayment");

	var firstPaymentSliderInst = new RangeSlider(
		$formSliderBlock.find(".RangeSlider"),
		{
			change: () => {
				pickupBanks();
			}
		},
		{
			slideEventCallback: (/*event, ui*/) => {
				let selectedFirstPayment = firstPaymentSliderInst.getValue();
				let selectedPrice = priceSliderInst.getValue();

				if (selectedFirstPayment >= selectedPrice * 0.9) {
					priceSliderInst.setHandlerPosition(Math.round(selectedFirstPayment * 1.15));
				}
			},
			$counter: $formSliderBlock.find(".MortgageSliders_sum")
		}
	);
}

{
	let $formSliderBlock = $calculatorForm.find(".MortgageSliders_option-price");

	var priceSliderInst = new RangeSlider(
		$formSliderBlock.find(".RangeSlider"),
		{
			change: () => {
				pickupBanks();
			}
		},
		{
			slideEventCallback: (/*event, ui*/) => {
				let selectedFirstPayment = firstPaymentSliderInst.getValue();
				let selectedPrice = priceSliderInst.getValue();

				if (selectedFirstPayment >= selectedPrice * 0.9) {
					firstPaymentSliderInst.setHandlerPosition(Math.round(selectedPrice * 0.9));
				}
			},
			$counter: $formSliderBlock.find(".MortgageSliders_sum")
		}
	);
}

{
	let $formSliderBlock = $calculatorForm.find(".MortgageSliders_option-duration");

	new RangeSlider(
		$formSliderBlock.find(".RangeSlider"),
		{
			change: () => {
				pickupBanks();
			}
		},
		{
			$counter: $formSliderBlock.find(".MortgageSliders_sum"),
			customCounterRender: v => {
				if (v === 1) {
					return "1 год";
				} else {
					return moment.duration(v, "years").humanize();
				}
			}
		}
	);
}

// SLIDERS END -------

pickupBanks();

// START: mobile features slider
new Swiper(".MortgageSlider", {
	speed: 400,
	pagination: {
		el: ".swiper-pagination",
		type: "bullets"
	}
});
