// TODO: remove all unsed ussignments
$(document).ready(function() {
	var webcamObj = {};
	var renderedPanoramaIndex = [];

	adjustGalleryHeight();

	window.onhashchange = function() {
		if (location.hash.length !== 0) {
			let id = Number(location.hash.match(/[0-9]+$/g).join());

			$(`[data-trigger-pack="${id}"]`)
				.find(".GallerySidebar_linkArea")
				.click();
		}
	};

	function initPanorama(el, swfUrl, xmlUrl) {
		embedpano({
			swf: swfUrl,
			xml: xmlUrl,
			target: el,
			html5: "prefer"
		});
	}

	let slider = new Swiper(".GalleryShow", {
		simulateTouch: false,
		allowTouchMove: false,
		navigation: {
			nextEl: ".swiper-button-next",
			prevEl: ".swiper-button-prev"
		},
		on: {
			slideChangeTransitionStart: function() {
				{
					let videoEl = document.querySelectorAll(".GalleryShow_video");

					let VimeoAPIInstance = new VimeoAPI().pause(videoEl);
				}

				if (this.activeIndex + 1 < 10) {
					$(".GalleryShow_counter").html("0" + (this.activeIndex + 1));
				} else {
					$(".GalleryShow_counter").html(this.activeIndex + 1);
				}

				let allSlidesArray = Object.values(this.slides);
				let currentSlideIndex = this.realIndex;
				let $currentSlide = allSlidesArray[currentSlideIndex];
				let currentPanoramaSlide = allSlidesArray.filter(el => {
					if ($(el).hasClass("GalleryShow_slide-panorama")) return el;
				})[currentSlideIndex];

				if ($(`.GalleryShow_slide:not([hidden]):eq(${currentSlideIndex})`).hasClass("GalleryShow_slide-panorama")) {
					if (
						!$(`.GallerySidebar_panoramaTrigger[data-trigger-panorama="${currentSlideIndex + 1}"]`).hasClass(
							"GallerySidebar_panoramaTrigger-active"
						)
					) {
						$(`.GallerySidebar_panoramaTrigger`).removeClass("GallerySidebar_panoramaTrigger-active");
						$(`.GallerySidebar_panoramaTrigger[data-trigger-panorama="${currentSlideIndex + 1}"]`).addClass(
							"GallerySidebar_panoramaTrigger-active"
						);
					}

					if (!renderedPanoramaIndex.includes(currentSlideIndex)) {
						renderedPanoramaIndex.push(currentSlideIndex);
						let el = `GalleryShow_slide-panorama${currentSlideIndex}`,
							swfUrl = $(`#${el}`).data("swf-path"),
							xmlUrl = $(`#${el}`).data("xml-path");
						initPanorama(el, swfUrl, xmlUrl);
					}
				}

				if ($(`.GalleryShow_slide:not([hidden]):eq(0)`).hasClass("GalleryShow_slide-webcam")) {
					if (!webcamObj[`cam${currentSlideIndex}`]) {
						webcamObj[`cam${currentSlideIndex}`] = new WowzaAPI(`GalleryShow_slide-webcam${currentSlideIndex}`, {
							sourceURL: document.getElementById(`GalleryShow_slide-webcam${currentSlideIndex}`).dataset.src
						});
					}

					$(`.GalleryShow_slide:not([hidden]).GalleryShow_slide-webcam`);
				}

				let $uploadInfoEl = $(".GalleryShow_infoText");
				let slidesPackId = $(".GalleryShow_slide:not([hidden])").data("slides-pack");

				let slideUploadInfo = [
					$(`.GalleryShow_slide[data-slides-pack="${slidesPackId}"]:eq(${currentSlideIndex})`).data("upload-author"),
					$(`.GalleryShow_slide[data-slides-pack="${slidesPackId}"]:eq(${currentSlideIndex})`).data("upload-date")
				];

				$uploadInfoEl.html(`
                Загрузил<br/>${slideUploadInfo[0]}<br/>${slideUploadInfo[1]}
            `);
			},
			init: function(event) {
				$(".GalleryShow_counter").html("01");
				panTriggerOnClickInit();
			}
		}
	});

	let $slideWithImg = $(
		".GalleryShow_slide:not(.GalleryShow_slide-webcam):not(.GalleryShow_slide-video):not(.GalleryShow_slide-panorama)"
	);

	$slideWithImg.click(() => {
		slider.slideNext();
	});

	let $initSlideUploadInfo = $slideWithImg[0];
	let initUploadInfo = [$($initSlideUploadInfo).data("upload-author"), $($initSlideUploadInfo).data("upload-date")];

	$(".GalleryShow_infoText").html(`Загрузил<br/>${initUploadInfo[0]}<br/>${initUploadInfo[1]}`);

	function panTriggerOnClickInit() {
		$(`.GallerySidebar_panoramaTrigger`).click(function() {
			let triggerId = $(this).data("trigger-panorama");
			$(`.GallerySidebar_panoramaTrigger`).removeClass(`GallerySidebar_panoramaTrigger-active`);
			$(this).addClass("GallerySidebar_panoramaTrigger-active");

			slider.slideTo(triggerId - 1);
		});
	}

	// Change active state of mobile tab
	GalleryMobileTabs(slider);

	// Sidebar scroll
	{
		let $sidebar = $(".GallerySidebar"),
			$window = $(window),
			isInited = false;

		function initNiceScrollVert(el) {
			el.niceScroll({
				cursorcolor: "linear-gradient(178deg, #86c98b, #7dcac5)",
				cursorfixedheight: 114,
				cursoropacitymin: 1,
				cursorwidth: 4,
				preventmultitouchscrolling: false,
				horizrailenabled: true,
				cursorborder: "0px solid transparent",
				scrollspeed: 200,
				cursorborderradius: "0"
			});
		}

		function initNiceScrollHor(el) {
			el.niceScroll({
				cursorcolor: "linear-gradient(178deg, #86c98b, #7dcac5)",
				cursorfixedheight: 114,
				cursoropacitymin: 1,
				cursorwidth: 5,
				preventmultitouchscrolling: false,
				horizrailenabled: true,
				cursorborder: "0px solid transparent",
				scrollspeed: 200,
				cursorborderradius: "0",
				railvalign: "top",
				railpadding: { top: 5, right: 0, left: 0, bottom: 0 }
			});
		}

		if ($window.width() > dimensionValue.tablet) {
			initNiceScrollVert($sidebar);
			isInited = true;
		} else if ($window.width() < dimensionValue.tablet && $window.width() > dimensionValue.mobile) {
			initNiceScrollHor($sidebar);
			isInited = true;
		}

		$(".nicescroll-cursors").addClass("GallerySidebarScroll");

		$(window).resize(function() {
			adjustGalleryHeight();

			let interval = setInterval(function() {
				$sidebar.getNiceScroll().resize();
				slider.update();
			}, 20);

			setTimeout(function() {
				clearInterval(interval);
			}, 500);

			if ($window.width() < dimensionValue.tablet && $window.width() > dimensionValue.mobile) {
				$sidebar.getNiceScroll().remove();
				initNiceScrollHor($sidebar);
				isInited = true;
			} else if ($window.width() > dimensionValue.tablet) {
				if (!isInited) {
					initNiceScrollVert($sidebar);
					isInited = true;
				}
			}
		});
	}

	// Block's text in sidebar clip with ellipsis
	{
		let $dotsText = $(".GallerySidebar_text"),
			dotsOptions = {
				fallbackToLetter: true,
				watch: true,
				height: 40
			};

		let dots;

		if ($(window).width() > dimensionValue.tablet) {
			dots = new Dots($dotsText, dotsOptions);
		}

		(function() {
			var $link = $(".GallerySidebar_more");

			$link.click(function(e) {
				e.preventDefault();

				setTimeout(function() {
					$(".GallerySidebar")
						.getNiceScroll()
						.resize();
				}, 300);

				if (
					$(this)
						.closest(".GallerySidebar_info")
						.hasClass("GallerySidebar_info-expanded")
				) {
					$(this).html("Подробнее");
					$(this)
						.closest(".GallerySidebar_info")
						.removeClass("GallerySidebar_info-expanded");

					dots.truncate();

					return false;
				}

				$(this).html("Скрыть");
				$(this)
					.closest(".GallerySidebar_info")
					.addClass("GallerySidebar_info-expanded");

				dots.restore();
			});
			$(window).resize(function() {
				$(".GallerySidebar")
					.getNiceScroll()
					.resize();
				$(".GallerySidebar_item .GallerySidebar_info").removeClass("GallerySidebar_info-expanded");

				if ($(window).width() > dimensionValue.tablet) {
					$link.html("Подробнее");
					$link.removeClass("GallerySidebar_more-shrink");

					if (!dots) {
						dots = new Dots($dotsText, dotsOptions);
					} else {
						dots.truncate();
					}
				} else {
					if (dots) {
						dots.restore();
					}
				}
			});

			$(".GallerySidebar_linkArea").click(function() {
				if ($(".GallerySidebar_info").hasClass("GallerySidebar_info-expanded")) {
					dots.truncate();

					$link.html("ÐŸÐ¾Ð´Ñ€Ð¾Ð±Ð½ÐµÐµ");

					$(".GallerySidebar_info").removeClass("GallerySidebar_info-expanded");
				}
			});
		})();
	}

	// Gallery fullscreen toggle
	{
		let $trigger = $(".GalleryShow_resize"),
			$sidebar = $(".GallerySidebar"),
			counter = 0;

		$trigger.click(function() {
			$(".Content").toggleClass("Content-galleryFull");

			$("body, html").animate(
				{
					scrollTop: 0
				},
				300
			);
			let intervalCounter = setInterval(function() {
				if (counter !== 3) {
					counter++;
				} else {
					counter = 0;
					clearInterval(intervalCounter);
				}
			}, 100);
			let intervalUpdatingSlider = setInterval(function() {
				if (counter === 3) {
					clearInterval(intervalUpdatingSlider);
				}
				slider.update();
				$sidebar.getNiceScroll().resize();
			}, 0);
		});
	}

	// Sidebar blocks toggle
	{
		let $el = $(".GallerySidebar_linkArea");

		$el.click(function() {
			{
				let videoEl = document.querySelectorAll(".GalleryShow_video");

				if (videoEl) {
					let VimeoAPIInstance = new VimeoAPI().pause(videoEl);
				}
			}

			if ($el.closest(".GallerySidebar_item").hasClass("GallerySidebar_item-webcam")) {
				if (!webcamObj[`cam0`]) {
					webcamObj[`cam0`] = new WowzaAPI(`GalleryShow_slide-webcam0`, {
						sourceURL: document.getElementById(`GalleryShow_slide-webcam0`).dataset.src
					});
				}
			}

			$el.closest(".GallerySidebar_item").removeClass("GallerySidebar_item-expanded");
			$(this)
				.closest(".GallerySidebar_item")
				.toggleClass("GallerySidebar_item-expanded");

			if (
				$(this)
					.closest(".GallerySidebar_item")
					.hasClass("GallerySidebar_item-video")
			) {
				$(".GalleryShow_toolpanel").addClass("GalleryShow_toolpanel-black");
			} else {
				$(".GalleryShow_toolpanel").removeClass("GalleryShow_toolpanel-black");
			}

			slider.slideTo(0);

			let $uploadInfoEl = $(".GalleryShow_infoText");
			let slidesPackId = $(this)
				.closest(".GallerySidebar_item")
				.data("trigger-pack");
			let slideUploadInfo = [
				$(`.GalleryShow_slide[data-slides-pack="${slidesPackId}"]:eq(0)`).data("upload-author"),
				$(`.GalleryShow_slide[data-slides-pack="${slidesPackId}"]:eq(0)`).data("upload-date")
			];

			$uploadInfoEl.html(`
                Загрузил<br/>
                ${slideUploadInfo[0]}
                <br/>
                ${slideUploadInfo[1]}
            `);

			setTimeout(function() {
				$(".GallerySidebar")
					.getNiceScroll()
					.resize();
			}, 300);

			if (
				$(this)
					.closest(".GallerySidebar_item")
					.hasClass("GallerySidebar_item-panorama")
			) {
				$('[data-trigger-panorama="1"]').addClass("GallerySidebar_panoramaTrigger-active");

				if (renderedPanoramaIndex.includes(0)) return;

				let firstPanoramaSlide = document.querySelectorAll(".GalleryShow_slide-panorama")[0];
				let el = `GalleryShow_slide-panorama0`,
					swfUrl = $(`#${el}`).data("swf-path"),
					xmlUrl = $(`#${el}`).data("xml-path");
				initPanorama("GalleryShow_slide-panorama0", swfUrl, xmlUrl);
				renderedPanoramaIndex.push(0);
			}
		});
	}

	// Changing pack of slides
	{
		$("[data-trigger-pack]").click(function() {
			$("[data-slides-pack]").attr("hidden", "true");
			$(`[data-slides-pack="${$(this).data("trigger-pack")}"]`).removeAttr("hidden");
			slider.update();
		});

		$(".GallerySidebar_item-panorama .GallerySidebar_linkArea").click(function() {
			if ($(window).width() >= dimensionValue.tablet) {
				setTimeout(() => {
					$(".GallerySidebar")
						.getNiceScroll(0)
						.doScrollTop(
							$(this)
								.closest(".GallerySidebar_item-panorama")
								.offset().top,
							200
						);
				}, 300);
			} else {
				$(".GallerySidebar")
					.getNiceScroll(0)
					.doScrollLeft(
						$(this)
							.closest(".GallerySidebar_item-panorama")
							.offset().left,
						200
					);
			}
		});

		if (location.hash.length !== 0) {
			let id = Number(location.hash.match(/[0-9]+$/g).join());

			$(`[data-trigger-pack="${id}"]`)
				.find(".GallerySidebar_linkArea")
				.click();
		}
	}

	//Mobile social expansion
	{
		let $trigger = $(".GalleryShow_socialExtend"),
			$social = $(".GalleryShow_socialLinks");

		showSocial($trigger, $social, "GalleryShow_socialLinks-active");
	}
});
