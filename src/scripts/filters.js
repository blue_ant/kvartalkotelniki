let cardsListBuilder = new PlanCardList();
let favList = new FavoritesList();

new FilterForm("#filterForm", {
	submitHandler: $filterForm => {
		// TODO: cache parsed json to instance attribute

		$.ajax({
			url: "https://kvartalkotelniki.ru/choose/params/",
			//url: '/ajax/get_typicals_example.json',
			dataType: "json",
			method: "GET",
			data: {
				action: "get_typicals"
			}
		})
			.done(jsonResponse => {
				// parse filters params to object
				let currentFiltersParams = prepareFilterFormValues($filterForm.serializeObject());

				let filteredPlans = jsonResponse;

				// sort plans array by cost
				filteredPlans = _.sortBy(filteredPlans, ["priceMin"]);

				// filter plans array
				//---- filter by bedrooms
				if (currentFiltersParams.bedrooms) {
					filteredPlans = _.filter(filteredPlans, pln => {
						let roomsNeed = currentFiltersParams.bedrooms;
						return _.includes(roomsNeed, pln.rooms);
					});
				}

				//---- filter by footage
				{
					filteredPlans = _.filter(filteredPlans, pln => {
						let result = false;

						let planFlats = pln.flats;
						if (planFlats) {
							let areaMinNeed = currentFiltersParams.area_from;
							let areaMaxNeed = currentFiltersParams.area_to;
							for (let i = planFlats.length - 1; i >= 0; i--) {
								if (_.inRange(planFlats[i].area, areaMinNeed, areaMaxNeed + 1)) {
									result = true;
									break;
								}
							}
						}

						return result;
					});
				}

				//---- filter by houses
				if (currentFiltersParams.house) {
					filteredPlans = _.filter(filteredPlans, pln => {
						let result = false;
						console.log('currentFiltersParams.house', _.intersection(currentFiltersParams.house, pln.houses))
						if (_.intersection(currentFiltersParams.house, pln.houses).length) {
							result = true;
						}
						return result;
					});
				}

				//---- filter by finish_option
				if (currentFiltersParams.finish_option !== undefined) {
					let activeType = currentFiltersParams.finish_option;
					const type = {
						0: 'finish-full',
						1: 'finish-white-box',
					};
					filteredPlans = _.filter(filteredPlans, pln => {
						return pln.flats.some(f => f[type[activeType]]);
					});
				}

				//---- filter by side of view
				if (currentFiltersParams.sides) {
					filteredPlans = _.filter(filteredPlans, pln => {
						let result = false;
						let planFlats = pln.flats;
						for (let i = planFlats.length - 1; i >= 0; i--) {
							if (_.intersection(currentFiltersParams.sides, planFlats[i].sides).length) {
								result = true;
								break;
							}
						}
						return result;
					});
				}

				//---- filter by furnish
				if (currentFiltersParams.finish === 1) {
					filteredPlans = _.filter(filteredPlans, pln => {
						return _.some(pln.flats, "finish");
					});
				} else if (currentFiltersParams.finish === 0) {
					filteredPlans = _.filter(filteredPlans, pln => {
						return _.some(pln.flats, ["finish", 0]);
					});
				}
				//---- filter by promo discount
				if (currentFiltersParams.discount == 1) {
					filteredPlans = _.filter(filteredPlans, pln => {
						return pln.discount || pln.strk;
					});
				}
				//---- filter by floor
				{
					filteredPlans = _.filter(filteredPlans, pln => {
						let result = false;

						let planFlats = pln.flats;
						let lowestFloor = currentFiltersParams.floor_from;
						let highestFloor = currentFiltersParams.floor_to;
						for (let i = planFlats.length - 1; i >= 0; i--) {
							if (_.inRange(planFlats[i].floor, lowestFloor, highestFloor + 1)) {
								result = true;
								break;
							}
						}
						return result;
					});
				}
				//---- filter by price
				{
					filteredPlans = _.filter(filteredPlans, pln => {
						let result = false;

						let planFlats = pln.flats;
						let priceMin = currentFiltersParams.price_from;
						let priceMax = currentFiltersParams.price_to;
						for (let i = planFlats.length - 1; i >= 0; i--) {
							if (_.inRange(planFlats[i].price, priceMin, priceMax + 1)) {
								result = true;
								break;
							}
						}
						return result;
					});
				}
				//---- filter by release date
				{
					filteredPlans = _.filter(filteredPlans, pln => {
						let result = false;

						let planFlats = pln.flats;
						let releaseDateFrom = currentFiltersParams.entry_date_from;
						let releaseDateTo = currentFiltersParams.entry_date_to;
						for (let i = planFlats.length - 1; i >= 0; i--) {
							if (_.inRange(planFlats[i].entryDate, releaseDateFrom, releaseDateTo + 1)) {
								result = true;
								break;
							}
						}
						return result;
					});
				}

				if (currentFiltersParams.sort_order === 2) {
					filteredPlans = _.reverse(filteredPlans);
				}

				// set favorites
				filteredPlans.forEach(pln => {
					pln.favorite = _.includes(favList.favorites, pln.id) ? 1 : 0;
				});

				// fix pics urls in development enviroment
				if (window.location.href.indexOf("/filters.html") != -1) {
					console.warn("rewrite json response for dev enviroment usage...");
					filteredPlans.forEach(flt => {
						flt.plan = "https://kvartalkotelniki.ru/" + flt.plan;
						flt.href = "/flat-type.html";
					});
				}

				// render flat list
				$("#flatTypesResultListContainer").html(cardsListBuilder.renderCards(filteredPlans));

				let planIdArr = [];
				let $planCards = $('.PlanCard');

				$planCards.each((index, item) => {
					planIdArr.push(String($(item).data('plan-id')));
				});

				window.criteo_q = window.criteo_q || [];
				var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
				window.criteo_q.push(
				 { event: "setAccount", account: 56959},
				 { event: "setEmail", email: "" },
				 { event: "setSiteType", type: deviceType},
				 { event: "viewList", item: planIdArr});

			})
			.fail(() => {
				alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
			});
		saveFilterFormInputs();
	}
});

$('#CheckboxGroup input:checkbox').click(function(){
	if ($(this).is(':checked')) {
		$('#CheckboxGroup input:checkbox').not(this).prop('checked', false);
	}
});

$("[data-toggle-filter-panel]").on("click", event => {
	event.preventDefault();
	$(".FiltersSection_rightCol").toggleClass("FiltersSection_rightCol-active");
});

// START: favorites toggling behaviour

$("#flatTypesResultListContainer").on("click", ".PlanCard_ico-star", event => {
	event.preventDefault();
	let $favLink = $(event.currentTarget);

	if ($favLink.hasClass("PlanCard_ico-active")) {
		favList.remove($favLink.data("flat-type-id"));
	} else {
		favList.add($favLink.data("flat-type-id"));
	}

	$favLink.toggleClass("PlanCard_ico-active");
});
// END: favorites toggling behaviour

//prevent firefox to get previous page from rendered cache
window.onunload = function() {};
